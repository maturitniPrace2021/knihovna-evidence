Knihovna – evidence, půjčování a vracení knih
=============================================
Vytvořte webové aplikace pro správu půjčených knih. V aplikace bude obsahovat přihlášení s notifikací přes školní účet. Dva typy uživatel – učitel ČJ a administrátor. Učitel může půjčovat a vracet knihy. Správce může administrovat obsah webu, přidávat nové tituly a odepisovat knihy, půjčovat knihy a vracet knihy za sebe i ostatní učitele.  

 
Adresa URL na webovou aplikaci: http://ddmknihovna.maweb.eu/Index.php

Přístup do správy: 

jméno: admin  
heslo: admin

Přístup pro běžné uživatele:

jméno: cernyf
heslo: heslo

jméno: prochazkat
heslo: heslo
