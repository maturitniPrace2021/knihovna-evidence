<?php
    require "vendor/autoload.php";
    require_once("include.php");

    $latte = new Latte\Engine;
    if(session_id() == ''){session_start();}

    $latte->setTempDirectory('src/temp');

    $params = ['filepath' => $filepath
    ];

    // kresli na výstup
    $latte->render('template/Index/Homeindex.latte', $params);

?>
