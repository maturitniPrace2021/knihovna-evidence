Web pro knihovnu knih.

2 typy užibstělů:
1.  Standartní
2.  Administrátor

Funkčnost pro daného uživatele:
1.  Standartní uživatel:
    1.1.    Zobrazení knihovny knih
    1.2.    Zobrazení zapůjčených knih
    1.3.    Zapůjčení knih/y
    1.4.    Vrácení knih/y
2.  Administrátor:
    2.1.    Zobrazení knihovny knih
    2.2.    Zobrazení zapůjčených knih
    2.3.    Zapůjčení knih/y
    2.4.    Vrácení knih/y
    2.5.    Evidace/přidání knihy
    2.6.    Odepsání knihy
    2.7.    Vrácení knihy za někoh
    2.8.    Zapůjčení knihy za někoho

    /*  <h2 n:foreach="$listBooks as $Books">
          <h3 n:foreach="$Books as $item">{​​​​$item->name}​​​​</h3>
        </h2>*/
