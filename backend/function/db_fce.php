<?php


set_time_limit(0);
if(session_id() == ''){session_start();}

/* dbs - database SELECT */
function dbsql($kod,$echo=0) {
  global $servername, $username, $password, $database;
  //Connect to database
  $link = mysqli_Connect($servername, $username, $password);
  $link->select_db($database);
  mysqli_set_charset($link, "utf8_czech_ci");
  // Check connection
  if (mysqli_connect_errno()) {
    echo '<script>alert("Failed to connect to MySQL: '.mysqli_connect_error().'")</script>';
    exit();
  }

  if($echo == 1){
    echo $kod;
    die;
  }

  #  mysqli_set_charset($link, "utf-8");

  $select = mysqli_query($link,$kod);

   mysqli_close($link);
}



function dbs_safe($kod,$unsafevariable,$echo=0) {
  global $servername, $username, $password, $database;
  $mysqli = new mysqli($servername, $username, $password, $database);
  // Check connection
  if ($mysqli -> connect_errno) {
    echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
    exit();
  }
  mysqli_set_charset($mysqli, "utf8_czech_ci");
  if($echo == 1){echo $kod;die;} //debuging zobrazeni promene
  $stmt = $mysqli->prepare($kod);
  //print_r($unsafevariable);die;

  $types = str_repeat('s', count($unsafevariable));

  $pole2 = array_values($unsafevariable);

  $stmt->bind_param($types, ...$pole2);

  if($stmt->execute()){}else echo '<script>alert("Error MySQL:'.$stmt->error.'")</script>';
  $select = $stmt->get_result();

  $count = mysqli_Num_Rows($select);
  $rows = mysqli_fetch_array($select);
  $r = 0;
  while($rows) {
    foreach ($rows as $key => $val) {
      $data[$r][$key] = $val;
    }
    $r++;
    $rows = mysqli_fetch_array($select);
  }
  if(isset($data)) return $data;

  $stmt->close();
  $mysqli->close();
}

/* dbs - database SELECT */
function dbs($kod,$echo=0) {
  global $servername, $username, $password, $database;
  //Connect to database
  $link = mysqli_Connect($servername, $username, $password);
  $link->select_db($database);
  mysqli_set_charset($link, "utf8_czech_ci");
  // Check connection
  if (mysqli_connect_errno()) {
    echo '<script>alert("Failed to connect to MySQL: '.mysqli_connect_error().'")</script>';
    exit();
  }

  if($echo == 1){
    echo $kod;
    die;
  }

  #  mysqli_set_charset($link, "utf-8");

  $select = mysqli_query($link,$kod);
  $count = mysqli_Num_Rows($select);
  $rows = mysqli_fetch_array($select);
  $r = 0;
  while($rows) {
    foreach ($rows as $key => $val) {
      $data[$r][$key] = $val;
    }
    $r++;
    $rows = mysqli_fetch_array($select);
  }
   if(isset($data)) return $data;

   mysqli_close($link);
}

/* získání počtu řádků */
function dbc($kod) {
  global $servername, $username, $password, $database;
  //Connect to database
  $link = mysqli_Connect($servername, $username, $password);
  $link->select_db($database);
  // Check connection
  if (mysqli_connect_errno()) {
    echo '<script>alert("Failed to connect to MySQL: '.mysqli_connect_error().'")</script>';
    exit();
  }


  $select = mysqli_query($link,$kod);
  return $count = mysqli_Num_Rows($select);

  mysqli_close($link);
}


function dbi($tab,$pole,$echo=0) {
  //logs
  $obj_logs = new Logs();
  $obj_logs->setTab($tab);
  $select = dbs("SELECT MAX(id) FROM ".$tab.";");
  $obj_logs->setId_tab($select[0][0]);
  $obj_logs->setinsert_update(0);
  foreach ($pole as $key => $value) {
    $obj_logs->setVariable($key);
    $obj_logs->setData($value);
    $obj_logs->insertLog();
  }

  global $servername, $username, $password, $database;
  //Connect to database
  $link = mysqli_Connect($servername, $username, $password);
  $link->select_db($database);
  // Check connection
  if (mysqli_connect_errno()) {
    echo '<script>alert("Failed to connect to MySQL: '.mysqli_connect_error().'")</script>';
    exit();
  }
  $count = count($pole);
  $r = 0;
  $cols = null;
  $vals = null;

  foreach ($pole as $key => $val) {
    strval($val);
    $r++;
    $cols .= $key;

    if($val == '0'){$vals .= "'0'";}else{
    if($val == 'null' or $val == "" or $val == '' or $val == "''" or $val == "wkejhiowbfkjafhwfoqwehf"){$vals .= " null ";}
    else{
    $vals .= "'".$val."'";}}

    if($r<>$count) {
      $cols .= ", ";
      $vals .= ", ";
    }

  }

  $sql = "INSERT INTO $tab ($cols) VALUES ($vals);";
  if($echo==1) {echo $sql; die;}
  $result = mysqli_query($link,$sql);
  if($result)
    return true;
  else{
    return false;
    echo '<script>alert("Error MySQL:'.mysqli_error($link).'")</script>';}

    mysqli_close($link);
}

function dbi_safe($tab,$pole,$echo=0) {
  //logs
  $obj_logs = new Logs();
  $obj_logs->setTab($tab);
  $select = dbs("SELECT MAX(id) FROM ".$tab.";");
  $obj_logs->setId_tab(6);
  $obj_logs->setinsert_update(0);
  foreach ($pole as $key => $value) {
    $obj_logs->setVariable($key);
    $obj_logs->setData($value);
    $obj_logs->insertLog();
  }

  global $servername, $username, $password, $database;
  $mysqli = new mysqli($servername, $username, $password, $database);
  // Check connection
  if ($mysqli -> connect_errno) {
    echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
    exit();
  }
  $count = count($pole);
  $r = 0;
  $cols = null;
  $vals = null;

  foreach ($pole as $key => $val) {
    strval($val);
    $r++;
    $cols .= $key;


    $vals .= "?";

    if($r<>$count) {
      $cols .= ", ";
      $vals .= ", ";
    }

  }

  $sql = "INSERT INTO $tab ($cols) VALUES ($vals);";
  if($echo==1) {echo $sql; die;}

  $stmt = $mysqli->prepare($sql);
  $types = str_repeat('s', count($pole));

  $pole2 = array_values($pole);

  $stmt->bind_param($types, ...$pole2);

  if($stmt->execute()){}else echo '<script>alert("Error MySQL:'.$stmt->error.')</script>';

  $stmt->close();
  $mysqli->close();
}


function dbu_v2($tab,$id_name,$id_value,$pole,$echo=0) {
  global $servername, $username, $password, $database;
  //Connect to database
  $link = mysqli_Connect($servername, $username, $password);
  $link->select_db($database);
  // Check connection
  if (mysqli_connect_errno()) {
    echo '<script>alert("Failed to connect to MySQL: '.mysqli_connect_error().'")</script>';
    exit();
  }

  foreach ($pole as $key => $value) {
    if(empty($value)){
      $value = null;
    }
  }

  $count = count($pole);
  $r = 0;
  $cols = null;
  $vals = null;
  foreach ($pole as $key => $val) {
    $r++;
    if($val == null){$vals .= $key." = null";}else{
    $vals .= $key." = '$val'";}
    if($r<>$count)
      $vals .= ", ";
  }

  $sql = "UPDATE $tab SET $vals WHERE $id_name = '$id_value';";
  if($echo==1) {echo $sql; die;}
    $result = mysqli_query($link,$sql);
    if($result)
      return true;
    else{
      return false;
      echo '<script>alert("Error MySQL:'.mysqli_error($link).'")</script>';}

      mysqli_close($link);
}

function dbu($tab,$id,$pole,$echo=0) {
  //logs
  $obj_logs = new Logs();
  $obj_logs->setTab($tab);
  $obj_logs->setId_tab($id);
  $obj_logs->setinsert_update(1);
  foreach ($pole as $key => $value) {
    $obj_logs->setVariable($key);
    $obj_logs->setData($value);
    $obj_logs->insertLog();
  }

  global $servername, $username, $password, $database;
  //Connect to database
  $link = mysqli_Connect($servername, $username, $password);
  $link->select_db($database);
  // Check connection
  if (mysqli_connect_errno()) {
    echo '<script>alert("Failed to connect to MySQL: '.mysqli_connect_error().'")</script>';
    exit();
  }

  $count = count($pole);
  $r = 0;
  $cols = null;
  $vals = null;
  foreach ($pole as $key => $val) {
    $r++;
    $vals .= $key." = '$val'";
    if($r<>$count)
      $vals .= ", ";
  }

  $sql = "UPDATE $tab SET $vals WHERE id = '$id';";
  if($echo==1) {echo $sql; die;}
    $result = mysqli_query($link,$sql);
    if($result)
      return true;
    else{
      return false;
      echo '<script>alert("Error MySQL:'.mysqli_error($link).'")</script>';}

      mysqli_close($link);
}

function dbu_safe($tab,$id,$pole,$echo=0) {
  //logs
  $obj_logs = new Logs();
  $obj_logs->setTab($tab);
  $obj_logs->setId_tab($id);
  $obj_logs->setinsert_update(1);
  foreach ($pole as $key => $value) {
    $obj_logs->setVariable($key);
    $obj_logs->setData($value);
    $obj_logs->insertLog();
  }

  global $servername, $username, $password, $database;
  $mysqli = new mysqli($servername, $username, $password, $database);
  // Check connection
  if ($mysqli -> connect_errno) {
    echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
    exit();
  }
  $count = count($pole);
  $r = 0;
  $cols = null;
  $vals = null;
  foreach ($pole as $key => $val) {
    $r++;
    $vals .= $key." = ?";
    if($r<>$count)
      $vals .= ", ";
  }

  $sql = "UPDATE $tab SET $vals WHERE id = '$id';";
  if($echo==1) {echo $sql; die;}

  $stmt = $mysqli->prepare($sql);
  $types = str_repeat('s', count($pole));

  $pole2 = array_values($pole);

  $stmt->bind_param($types, ...$pole2);

  if($stmt->execute()){}else echo '<script>alert("Error MySQL:'.$stmt->error.')</script>';
  $select = $stmt->get_result();

  $stmt->close();
  $mysqli->close();
}


function dbd($tab,$id,$echo=0) {
  global $servername, $username, $password, $database;
  //Connect to database
  $link = mysqli_Connect($servername, $username, $password);
  $link->select_db($database);
  // Check connection
  if (mysqli_connect_errno()) {
    echo '<script>alert("Failed to connect to MySQL: '.mysqli_connect_error().'")</script>';
    exit();
  }


  $sql = "DELETE FROM $tab WHERE id = '".$id."';";
  if($echo==1) {echo $sql; die;}
   if (mysqli_query($link, $sql)) {
   } else {
      echo '<script>alert("Error MySQL:'.mysqli_error($link).'")</script>';
   }

   mysqli_close($link);
}

function dbd_safe($tab,$id,$echo=0) {
  global $servername, $username, $password, $database;
  $mysqli = new mysqli($servername, $username, $password, $database);
  // Check connection
  if ($mysqli -> connect_errno) {
    echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
    exit();
  }

  $sql = "DELETE FROM $tab WHERE id = ?;";
  if($echo==1) {echo $sql; die;}
  $stmt = $mysqli->prepare($sql);

  $stmt->bind_param('s', $id);

  if($stmt->execute()){}else echo '<script>alert("Error MySQL:'.$stmt->error.')</script>';

  $stmt->close();
  $mysqli->close();
}



?>
