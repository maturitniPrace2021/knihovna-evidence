<?php


class Logs {
    private $id;
    private $tab;
    private $id_tab;
    private $variable;
    private $data;
    private $time;
    private $insert_update;

    private $table = "logs";

    private $attributes = array("id","id_tab","tab","data", "variable", "insert_update");

    public function __construct(){
    }

    public function getId(){
        return $this->id;
    }
    public function getTab(){
        return $this->tab;
    }
    public function getId_tab(){
        return $this->id_tab;
    }
    public function getVariable(){
        return $this->variable;
    }
    public function getData(){
        return $this->data;
    }
    public function getInsert_update(){
        return $this->insert_update;
    }


    public function setTab($tab){
        $this->tab = $tab;
    }
    public function setId_tab($id_tab){
        $this->id_tab = $id_tab;
    }
    public function setVariable($variable){
        $this->variable = $variable;
    }
    public function setData($data){
        $this->data = $data;
    }
    public function setinsert_update($insert_update){
        $this->insert_update = $insert_update;
    }


    public function setAllAttributesby_Id($id){
      $sql = "SELECT * FROM ".$this->table." WHERE id = ?";
      $select = dbs_safe($sql,array($id)); //print_r($select[0]);die; //TEST
      if (!empty($select[0])) {
        foreach ($this->attributes as $value_att) {
          foreach ($select[0] as $key_se => $value_se) {
            if ($key_se == $value_att) {
              $this->$value_att = $value_se;
            }
          }
        }
      }
      else {
        foreach ($this->attributes as $value_att) {
          $this->$value_att = "žádný záznam";
        }
      }

    }


    public function insertLog(){
      dbsql("INSERT INTO logs (tab,id_tab,variable,data,insert_update) VALUES('".$this->tab."','".$this->id_tab."','".$this->variable."', '".$this->data."', '".$this->insert_update."');");
    }

    public function getAlllogs(){
      $sql = "SELECT id,id_tab,tab FROM ".$this->table."";
      $select = dbs($sql);

      return $select;
    }
    public function getsql($sql){
      $select = dbs($sql);

      return $select;
    }
    public function getsqlsafe($sql,$unsafevariables){
      $select = dbs_safe($sql,$unsafevariables);

      return $select;
    }


}
?>
