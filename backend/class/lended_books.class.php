<?php


class Lended_books {
    private $id;
    private $id_books;
    private $id_users;
    private $date_lend;
    private $date_return;
    private $number;
    private $active;
    private $changes;

    private $tab = "lended_books";

    private $attributes = array("id","id_books","id_users","date_lend", "date_return", "actual_date_return", "number", "active", "changes");

    public function __construct(){
    }

    public function getId(){
        return $this->id;
    }
    public function getId_books(){
        return $this->id_books;
    }
    public function getId_users(){
        return $this->id_users;
    }
    public function getDate_lend(){
        return $this->date_lend;
    }
    public function getDate_return(){
        return $this->date_return;
    }
    public function getActual_date_return(){
        return $this->actual_date_return;
    }
    public function getNumber(){
        return $this->number;
    }
    public function getActive(){
        return $this->active;
    }
    public function getChanges(){
        return $this->changes;
    }

    public function setId_books($id_books){
        $this->id_books = $id_books;
    }
    public function setId_users($id_users){
        $this->id_users = $id_users;
    }
    public function setDate_lend($date_lend){
        $this->date_lend = $date_lend;
    }
    public function setDate_return($date_return){
        $this->date_return = $date_return;
    }
    public function setActual_date_return($actual_date_return){
        $this->actual_date_return = $actual_date_return;
    }
    public function setNumber($number){
        $this->number = $number;
    }
    public function setActive($active){
        $this->active = $active;
    }


    public function setAllAttributesby_Id($id){
      $sql = "SELECT * FROM ".$this->tab." WHERE id = ?";
      $select = dbs_safe($sql,array($id)); //print_r($select[0]);die; //TEST
      if (!empty($select[0])) {
        foreach ($this->attributes as $value_att) {
          foreach ($select[0] as $key_se => $value_se) {
            if ($key_se == $value_att) {
              $this->$value_att = $value_se;
            }
          }
        }
      }
      else {
        foreach ($this->attributes as $value_att) {
          $this->$value_att = "žádný záznam";
        }
      }

    }


    public function lendbook(){
      $this->actual_date_return = null;
      $this->active = 1;
      $attributes = array("id_books","id_users", "date_lend", "date_return", "actual_date_return", "number", "active");
      foreach ($this->attributes as $key) {
        foreach ($attributes as $key2) {
          if ($key == $key2) {
            $variables[$key] = $this->$key;
          }
        }
      }

      dbi_safe($this->tab, $variables);

      $sql = "SELECT MAX(id) FROM ".$this->tab."";
      $select = dbs($sql);
      $this->setAllAttributesby_Id($select[0][0]); //print_r($select[0][0]);die; //TEST

      //odecitani ze skladu
      $obj_books = new Books();
      $obj_books->setAllAttributesby_Id($this->id_books);
      $obj_books->setWarehouse(($obj_books->getWarehouse())-($this->number));
      if ($obj_books->getWarehouse()<=-1) {
        echo '<script>alert("Chyba půjčuje se více než je na skladě")</script>';
         die;
      }
      $obj_books->updateBook($this->id_books);
    }


    public function returnbook($id, $actual_date_return){
      $variables['active'] = '0';
      $variables['actual_date_return'] = $actual_date_return;
      dbu_safe($this->tab, $id, $variables);

      $this->setAllAttributesby_Id($id);

      //pricitani ze skladu
      $obj_books = new Books();
      $obj_books->setAllAttributesby_Id($this->id_books);
      $obj_books->setWarehouse(($obj_books->getWarehouse())+($this->number));
      if ($obj_books->getWarehouse()<0) {
        echo '<script>alert("Chyba půjčuje se více než je na skladě")</script>';
         die;
      }
      $obj_books->updateBook($this->id_books);
    }


    public function deleteLended_book($id){
      $this->setAllAttributesby_Id($id);

      dbd_safe($this->tab, $id);
    }


    public function getAllLended_books(){
      $sql = "SELECT * FROM ".$this->tab." WHERE active = 1";
      $select = dbs($sql);

      return $select;
    }


    public function getFilterLended_booksby_idbooks($id_books){
      $sql = "SELECT * FROM ".$this->tab." WHERE id_books = '".$id_books."' AND active = 1";
      $select = dbs($sql);

      return $select;
    }

    public function getFilterLended_booksby_sql($sql){
      $select = dbs($sql);

      return $select;
    }

    public function getFilterLended_bookswhere($where){
      $sql = "SELECT * FROM ".$this->tab." WHERE active = 1 ".$where."";
      $select = dbs($sql);

      return $select;
    }

    public function getFilterLended_booksby_idusers($id_users,$where=""){
      $sql = "SELECT * FROM ".$this->tab." WHERE id_users = '".$id_users."' AND active = 1 ".$where."";
      $select = dbs($sql);

      return $select;
    }

    public function getFilterLended_books_active(){
      $sql = "SELECT * FROM ".$this->tab." WHERE active = 1";
      $select = dbs($sql);

      return $select;
    }

    public function getFilterLended_books_inactive(){
      $sql = "SELECT * FROM ".$this->tab." WHERE active = 0";
      $select = dbs($sql);

      return $select;
    }

    public function getFilterLended_books_sql($sql){
      $select = dbs($sql);

      return $select;
    }

    public function getFilterLended_books_sqlsafe($sql, $unsafevariables){
      $select = dbs_safe($sql,$unsafevariables);

      return $select;
    }

    public function getIdbooksby_id($id){
      $sql = "SELECT id_books FROM ".$this->tab." WHERE id = ".$id."";
      $select = dbs($sql);
      return $select[0][0];
    }



    public function detailLended_bookby_Id($id){
      $this->setAllAttributesby_Id($id);
      foreach ($this->attributes as $key) {
        $return[$key] = $this->$key;
      }

      return $return;
    }


}
?>
