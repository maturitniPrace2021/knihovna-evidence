<?php


class Books {
    private $id;
    private $name;
    private $author;
    private $isbn;
    private $warehouse;
    private $changes;

    private $tab = "books";

    private $attributes = array("id","name","author","isbn","warehouse", "changes");

    public function __construct(){
    }

    public function getId(){
        return $this->id;
    }
    public function getName(){
        return $this->name;
    }
    public function getAuthor(){
        return $this->author;
    }
    public function getIsbn(){
        return $this->isbn;
    }
    public function getWarehouse(){
        return $this->warehouse;
    }
    public function getChanges(){
        return $this->changes;
    }


    public function setName($name){
        $this->name = $name;
    }
    public function setAuthor($author){
        $this->author = $author;
    }
    public function setIsbn($isbn){
        $this->isbn = $isbn;
    }
    public function setWarehouse($warehouse){
        $this->warehouse = $warehouse;
    }


    public function setAllAttributesby_Id($id){
      $sql = "SELECT * FROM ".$this->tab." WHERE id = ?";
      $select = dbs_safe($sql,array($id)); //print_r($select[0]);die; //TEST
      if (!empty($select[0])) {
        foreach ($this->attributes as $value_att) {
          foreach ($select[0] as $key_se => $value_se) {
            if ($key_se == $value_att) {
              $this->$value_att = $value_se;
            }
          }
        }
      }
      else {
        foreach ($this->attributes as $value_att) {
          $this->$value_att = "žádný záznam";
        }
      }

    }


    public function insertBook(){
      $attributes = array("name", "author","isbn", "warehouse");
      foreach ($this->attributes as $key) {
        foreach ($attributes as $key2) {
          if ($key == $key2) {
            $variables[$key] = $this->$key;
          }
        }
      }
      dbi_safe($this->tab, $variables);

      $sql = "SELECT MAX(id) FROM ".$this->tab."";
      $select = dbs($sql);
      $this->setAllAttributesby_Id($select[0][0]); //print_r($select[0][0]);die; //TEST
    }


    public function updateBook($id){
      $attributes = array("name", "author","isbn", "warehouse");
      foreach ($this->attributes as $key) {
        foreach ($attributes as $key2) {
          if ($key == $key2) {
            $variables[$key] = $this->$key;
          }
        }
      }
      dbu_safe($this->tab, $id, $variables);

      $this->setAllAttributesby_Id($id);
    }


    public function deleteBook($id){
      $this->setAllAttributesby_Id($id);

      dbd_safe($this->tab, $id);
    }


    public function getAllBooks(){
      $sql = "SELECT * FROM ".$this->tab." ORDER BY name";
      $select = dbs($sql);
      if (empty($select[0][0])) {
        $select[0]['name'] = "žádná data";
      }
      return $select;
    }


    public function getFilterBooksby_author($author){
      $sql = "SELECT * FROM ".$this->tab." WHERE author = '".$author."'";
      $select = dbs($sql);

      return $select;
    }

    public function getFilterBooks_sqlWHERE($where){
      $sql = "SELECT * FROM ".$this->tab." WHERE ".$where."";
      $select = dbs($sql);
      if (empty($select[0][0])) {
        $select[0]['name'] = "žádná data";
      }
      return $select;
    }

    public function getFilterBooks_sqlWHEREsafe($where,$unsafevariables){
      $sql = "SELECT * FROM ".$this->tab." WHERE ".$where."";
      $select = dbs_safe($sql,$unsafevariables);
      if (empty($select[0][0])) {
        $select[0]['name'] = "žádná data";
      }
      return $select;
    }

    public function getFilterBooksby_user($id_users, $where = "", $where2 =""){
      $i = 0;
      $obj_lendedbooks = new Lended_books();
      $lendedbooks = $obj_lendedbooks->getFilterLended_booksby_idusers($id_users,$where2);
      if (!empty($lendedbooks[0][0])){
        foreach ($lendedbooks as $key => $value) {
          $sql = "SELECT * FROM ".$this->tab." WHERE id = ".$value['id_books']." ".$where."";
          $select = dbs($sql);
            if ($value['id_books'] == $select[0]['id']) {
              $obj_users = new Users();
              $obj_users->setAllAttributesby_Id($value['id_users']);
              $users_name = $obj_users->getName();
              $return[$i] = array_merge($select[0], $value);
              $return[$i++]['users_name'] = $users_name;
            }
        }
      }
      else
      {
        $return[0]['name'] = "žádná data";
      }

      if (empty($return)) {
        $return[0]['name'] = "žádná data";
      }
      return $return;
    }

    public function getFilterBooksby_usersafe($id_users, $where = "", $where2 ="",$unsafevariables){
      $i = 0;
      $obj_lendedbooks = new Lended_books();
      $lendedbooks = $obj_lendedbooks->getFilterLended_booksby_idusers($id_users,$where2);
      if (!empty($lendedbooks[0][0])){
        foreach ($lendedbooks as $key => $value) {
          $sql = "SELECT * FROM ".$this->tab." WHERE id = ".$value['id_books']." ".$where."";
          $select = dbs_safe($sql,$unsafevariables);
            if ($value['id_books'] == $select[0]['id']) {
              $obj_users = new Users();
              $obj_users->setAllAttributesby_Id($value['id_users']);
              $users_name = $obj_users->getName();
              $return[$i] = array_merge($select[0], $value);
              $return[$i++]['users_name'] = $users_name;
            }
        }
      }
      else
      {
        $return[0]['name'] = "žádná data";
      }

      if (empty($return)) {
        $return[0]['name'] = "žádná data";
      }
      return $return;
    }

    public function getFilterBooksby_alllendedbooks($where = "",$where2= ""){
      $i = 0;
      $obj_lendedbooks = new Lended_books();
      $lendedbooks = $obj_lendedbooks->getFilterLended_bookswhere($where2);
      if (empty($lendedbooks)) {
        $return[0]['name'] = "žádná data";
      }
      else{
        foreach ($lendedbooks as $key => $value) {
          $sql = "SELECT * FROM ".$this->tab." WHERE id = ".$value['id_books']." ".$where."";
          $select = dbs($sql);
            if ($value['id_books'] == $select[0]['id']) {
              $obj_users = new Users();
              $obj_users->setAllAttributesby_Id($value['id_users']);
              $users_name = $obj_users->getName();
              $return[$i] = array_merge($select[0], $value);
              $return[$i++]['users_name'] = $users_name;
            }
        }
      }
      if (empty($return[0][0])) {
        $return[0]['name'] = "žádná data";
      }
      if (empty($return)) {
        $return[0]['name'] = "žádná data";
      }
      return $return;
    }

    public function getFilterBooksby_alllendedbookssafe($where = "",$where2= "",$unsafevariables){
      $i = 0;
      $obj_lendedbooks = new Lended_books();
      $lendedbooks = $obj_lendedbooks->getFilterLended_bookswhere($where2);
      foreach ($lendedbooks as $key => $value) {
        $sql = "SELECT * FROM ".$this->tab." WHERE id = ".$value['id_books']." ".$where."";
        $select = dbs_safe($sql, $unsafevariables);
          if ($value['id_books'] == $select[0]['id']) {
            $obj_users = new Users();
            $obj_users->setAllAttributesby_Id($value['id_users']);
            $users_name = $obj_users->getName();
            $return[$i] = array_merge($select[0], $value);
            $return[$i++]['users_name'] = $users_name;
          }
      }
      if (empty($return[0][0])) {
        $return[0]['name'] = "žádná data";
      }
      if (empty($return)) {
        $return[0]['name'] = "žádná data";
      }
      return $return;
    }


    public function detailBookby_Id($id){
      $this->setAllAttributesby_Id($id);
      foreach ($this->attributes as $key) {
        $return[$key] = $this->$key;
      }

      return $return;
    }


}
?>
