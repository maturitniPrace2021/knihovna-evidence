<?php


class Notes {
    private $id;
    private $id_lendedbooks;
    private $id_books;
    private $id_users;
    private $note;
    private $lend_return;
    private $changes;

    private $tab = "notes";

    private $attributes = array("id","id_lendedbooks","id_books","id_users", "note","lend_return", "changes");

    public function __construct(){
    }

    public function getId(){
        return $this->id;
    }
    public function getId_lendedbooks(){
        return $this->id_lendedbooks;
    }

    public function getId_books(){
        return $this->id_books;
    }

    public function getId_users(){
        return $this->id_users;
    }

    public function getNote(){
        return $this->note;
    }
    public function getLend_return(){
        return $this->lend_return;
    }
    public function getChanges(){
        return $this->changes;
    }

    public function setId_books($id_books){
        $this->id_books = $id_books;
    }
    public function setId_users($id_users){
        $this->id_users = $id_users;
    }
    public function setId_lendedbooks($id_lendedbooks){
        $this->id_lendedbooks = $id_lendedbooks;
    }
    public function setNote($note){
        $this->note = $note;
    }
    public function setLend_return($lend_return){
        $this->lend_return = $lend_return;
    }


    public function setAllAttributesby_Id($id){
      $sql = "SELECT * FROM ".$this->tab." WHERE id = ?";
      $select = dbs_safe($sql,array($id)); //print_r($select[0]);die; //TEST
      if (!empty($select[0])) {
        foreach ($this->attributes as $value_att) {
          foreach ($select[0] as $key_se => $value_se) {
            if ($key_se == $value_att) {
              $this->$value_att = $value_se;
            }
          }
        }
      }
      else {
        foreach ($this->attributes as $value_att) {
          $this->$value_att = "žádný záznam";
        }
      }

    }

    public function setAllAttributesby_Idlendedbooks($id){
      $sql = "SELECT * FROM ".$this->tab." WHERE id_lendedbooks = ?";
      $select = dbs_safe($sql,array($id));
      foreach ($select as $key => $value) {
        foreach ($attributes as $key2) {
          if ($value == $key2) {
            $this->$value = $key2;
          }
        }
      }
    }


    public function insertNote(){
      if ($this->note != '') {
        $variables['id_lendedbooks'] = $this->id_lendedbooks;
        $variables['note'] = $this->note;
        $variables['lend_return'] = $this->lend_return;

        $obj_lendedbooks = new Lended_books();
        $variables['id_books'] = $obj_lendedbooks->getIdbooksby_id($variables['id_lendedbooks']);
        $variables['id_users'] = $_SESSION["user_id"];

        dbi_safe($this->tab, $variables);

        $sql = "SELECT MAX(id) FROM ".$this->tab."";
        $select = dbs($sql);
        $this->setAllAttributesby_Id($select[0][0]); //print_r($select[0][0]);die; //TEST
      }
    }


    public function updateNote(){
      $attributes = array("id_lendedbooks", "note", "lend_return");
      foreach ($this->attributes as $key) {
        foreach ($attributes as $key2) {
          if ($key == $key2) {
            $variables[$key] = $this->$key;
          }
        }
      }
      $obj_lendedbooks = new Lended_books();
      $variables['id_books'] = $obj_lendedbooks->getIdbooksby_id($variables['id_lendedbooks']);
      $variables['id_users'] = $_SESSION["user_id"];

      dbu_safe($this->tab, $id, $variables);

      $this->setAllAttributesby_Id($id);
    }


    public function deleteNote($id){
      $this->setAllAttributesby_Id($id);

      dbd_safe($this->tab, $id);
    }


    public function getLendNotesby_idbooks($id){
      $sql = "SELECT notes.id, notes.id_lendedbooks, notes.id_books, notes.id_users, notes.note, notes.lend_return, notes.changes ,users.name AS username FROM ".$this->tab."  LEFT JOIN users ON users.id=notes.id_users WHERE id_books = '".$id."' AND lend_return = 0 ORDER BY notes.changes DESC;";
      $select = dbs($sql);
      if(!empty($select[0][0])){
      $return = $select;
      }
      else{
        $return[0]['note'] = "žádné poznámky";
      }
      return $return;
    }

    public function getReturnNotesby_idbooks($id){
      $sql = "SELECT notes.id, notes.id_lendedbooks, notes.id_books, notes.id_users, notes.note, notes.lend_return, notes.changes ,users.name AS username FROM ".$this->tab."  LEFT JOIN users ON users.id=notes.id_users WHERE id_books = '".$id."' AND lend_return = 1 ORDER BY notes.changes DESC;";
      $select = dbs($sql);
      if(!empty($select[0][0])){
      $return = $select;
      }
      else{
        $return[0]['note'] = "žádné poznámky";
      }
      return $return;
    }

    public function getFilterNotes_sql($sql){
      $select = dbs($sql);

      return $select;
    }

    public function detailNoteby_Id($id){
      $this->setAllAttributesby_Id($id);
      foreach ($this->attributes as $key) {
        $return[$key] = $this->$key;
      }

      return $return;
    }


}
?>
