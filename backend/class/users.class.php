<?php


class Users {
    private $id;
    private $name;
    private $username;
    private $admin;
    private $password;
    private $changes;

    private $tab = "users";

    private $attributes = array("id","username","name","password", "admin");

    public function __construct(){
    }

    public function getId(){
        return $this->id;
    }
    public function getName(){
        return $this->name;
    }
    public function getUsername(){
        return $this->username;
    }
    public function getAdmin(){
        return $this->admin;
    }
    public function getPassword(){
        return $this->password;
    }


    public function setName($name){
        $this->name = $name;
    }
    public function setUsername($username){
        $this->username = $username;
    }
    public function setAdmin($admin){
        $this->admin = $admin;
    }
    public function setPassword($password){
        $this->password = $password;
    }


    public function setAllAttributesby_Id($id){
      $sql = "SELECT * FROM ".$this->tab." WHERE id = ?";
      $select = dbs_safe($sql,array($id)); //print_r($select[0]);die; //TEST
      if (!empty($select[0])) {
        foreach ($this->attributes as $value_att) {
          foreach ($select[0] as $key_se => $value_se) {
            if ($key_se == $value_att) {
              $this->$value_att = $value_se;
            }
          }
        }
      }
      else {
        foreach ($this->attributes as $value_att) {
          $this->$value_att = "žádný záznam";
        }
      }

    }


    public function insertUser(){
      $attributes = array("name", "username", "admin", "password");
      foreach ($this->attributes as $key) {
        foreach ($attributes as $key2) {
          if ($key == $key2) {
            $variables[$key] = $this->$key;
          }
        }
      }
      dbi_safe($this->tab, $variables);

      $sql = "SELECT MAX(id) FROM ".$this->tab."";
      $select = dbs($sql);
      $this->setAllAttributesby_Id($select[0][0]); //print_r($select[0][0]);die; //TEST
    }


    public function updateUser($id){
      $attributes = array("name", "username", "admin", "password");
      foreach ($this->attributes as $key) {
        foreach ($attributes as $key2) {
          if ($key == $key2) {
            $variables[$key] = $this->$key;
          }
        }
      }
      dbu_safe($this->tab, $id, $variables);

      $this->setAllAttributesby_Id($id);
    }


    public function deleteUser($id){
      $this->setAllAttributesby_Id($id);

      dbd_safe($this->tab, $id);
    }


    public function getAllUsers(){
      $sql = "SELECT id,username,name FROM ".$this->tab."";
      $select = dbs($sql);

      return $select;
    }
    public function getsql($sql){
      $select = dbs($sql);

      return $select;
    }
    public function getsqlsafe($sql,$unsafevariables){
      $select = dbs_safe($sql,$unsafevariables);

      return $select;
    }

    public function getFilterUsers_sqlwheresafe($where,$unsafevariables){
      $sql = "SELECT id,username,name,admin,password FROM ".$this->tab." WHERE ".$where."";
      $select = dbs_safe($sql,$unsafevariables);

      return $select;
      if (!empty($select[0][0])) {
      $this->setAllAttributesby_Id($select[0]['id']);
      }
    }

    public function getFilterUsers_sqlwhere($where){
      $sql = "SELECT id,username,name,admin FROM ".$this->tab." WHERE ".$where."";
      $select = dbs($sql);

      return $select;
      if (!empty($select[0][0])) {
      $this->setAllAttributesby_Id($select[0]['id']);
      }
    }


    public function detailBookby_Id($id){
      $this->setAllAttributesby_Id($id);
      foreach ($this->attributes as $key) {
        $return[$key] = $this->$key;
      }

      return $return;
    }


}
?>
