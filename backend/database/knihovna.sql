-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1:3306
-- Vytvořeno: Úte 13. dub 2021, 13:05
-- Verze serveru: 5.7.31
-- Verze PHP: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `book_library`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `books`
--

DROP TABLE IF EXISTS `books`;
CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `author` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `isbn` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `warehouse` int(11) NOT NULL DEFAULT '0',
  `changes` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `nazev` (`name`),
  KEY `autor` (`author`),
  KEY `pocet_sklad` (`warehouse`),
  KEY `uprava` (`changes`),
  KEY `isbn` (`isbn`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `books`
--

INSERT INTO `books` (`id`, `name`, `author`, `isbn`, `warehouse`, `changes`) VALUES
(1, 'Pes BaskirvelskÃ½', 'Arthur Conan Doyle', '1243-1324-1234-1432', 1, '2021-04-12 13:52:49'),
(3, 'Anna Kareninaaaa', 'Lev NikolajeviÄ Tolstoj', '1243-1324-1234-1432', 14, '2021-04-10 19:35:57'),
(25, 'Ä›Å¡Å¡Å™Å¡Ä+Ä›Å™Å¾Ã­Å™Å¾Å¡', 'Ä›Å¡ÄÅ™Ã½Å¡Ä›Ã­ÄÅ™', '12445236', 2, '2021-04-13 12:57:28'),
(22, 'zsfasdfas', 'asdfaf', 'asdfasdf', 0, '2021-04-08 14:35:02'),
(26, 'Ä›Å¡ÄÅ™Ä›Å¡ÄÅ™Ä›Å¾', 'Ä›Å¡ÄÅ™Ä›Å¡ÄÅ™Ä›', '12435234', 0, '2021-04-12 10:26:47'),
(27, 'Å¡Å¾Å™Å¡Å¾Å¡Äfgsdfg', 'Å¡ÄÅ™Å¾Å¡ÄÅ™Å¾Å¡', 'Å¡ÄÅ™Å¡Å¾Å¡ÄÅ™Å¾', 0, '2021-04-13 12:57:20'),
(28, 'ÄÅ¾Å¡ÄÅ™Å¾', 'Å¡ÄÅ™Å¾Å¡ÄÅ™Å¾', 'Å¡ÄÅ™Å¾Å¡ÄÅ™Å¾', 1, '2021-04-13 12:49:28'),
(29, 'ÄÅ¾Å¡ÄÅ™Å¾', 'Å¡ÄÅ™Å¾Å¡ÄÅ™Å¾', 'Å¡ÄÅ™Å¾Å¡ÄÅ™Å¾', 1, '2021-04-13 12:50:59'),
(30, 'ÄÅ¾Å¡ÄÅ™Å¾', 'Å¡ÄÅ™Å¾Å¡ÄÅ™Å¾', 'Å¡ÄÅ™Å¾Å¡ÄÅ™Å¾', 1, '2021-04-13 12:53:34'),
(31, 'Ä›Å¡ÄÅ¡ÄÅ¾', 'Å¡ÄÅ™Å¾Å¡ÄÅ¾', 'Å¡ÄÅ™Å¾Å¡ÄÅ™Å¾', 1, '2021-04-13 12:53:48'),
(32, 'Ä›Å¡ÄÅ¡ÄÅ¾', 'Å¡ÄÅ™Å¾Å¡ÄÅ¾', 'Å¡ÄÅ™Å¾Å¡ÄÅ™Å¾', 1, '2021-04-13 12:56:34'),
(33, 'ÄÅ¡Å¾Å™Å¡Å¾', 'Å¡ÄÅ™Å¾Å¡ÄÅ™Å¾', 'ÄÅ¡Å™Å¾Å¡ÄÅ¾', 1, '2021-04-13 12:57:02');

-- --------------------------------------------------------

--
-- Struktura tabulky `law`
--

DROP TABLE IF EXISTS `law`;
CREATE TABLE IF NOT EXISTS `law` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `law`
--

INSERT INTO `law` (`id`, `text`) VALUES
(1, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasdll');

-- --------------------------------------------------------

--
-- Struktura tabulky `lended_books`
--

DROP TABLE IF EXISTS `lended_books`;
CREATE TABLE IF NOT EXISTS `lended_books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_books` int(11) DEFAULT NULL,
  `id_users` int(11) DEFAULT NULL,
  `date_lend` date DEFAULT NULL,
  `date_return` date DEFAULT NULL,
  `actual_date_return` date DEFAULT NULL,
  `number` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `changes` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_knihy` (`id_books`),
  KEY `jmeno` (`id_users`),
  KEY `datum_pujceni` (`date_lend`),
  KEY `datum_vraceni` (`date_return`),
  KEY `pocet` (`number`),
  KEY `aktivni` (`active`),
  KEY `uprava` (`changes`),
  KEY `actual_date_return` (`actual_date_return`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `lended_books`
--

INSERT INTO `lended_books` (`id`, `id_books`, `id_users`, `date_lend`, `date_return`, `actual_date_return`, `number`, `active`, `changes`) VALUES
(1, 3, 24, '2021-04-10', '2021-07-10', '2021-04-11', 1, 0, '2021-04-11 18:17:16'),
(2, 3, 24, '2021-04-10', '2021-07-10', '2021-04-11', 1, 0, '2021-04-11 18:17:16'),
(3, 1, 24, '2021-04-11', '2021-07-11', '2021-04-11', 1, 0, '2021-04-11 18:17:16'),
(4, 1, 27, '2021-04-11', '2021-07-11', NULL, 1, 1, '2021-04-11 21:33:22'),
(5, 1, 18, '2021-04-12', '2021-07-12', NULL, 1, 1, '2021-04-12 13:52:49'),
(6, 25, 27, '2021-04-13', '2021-07-13', NULL, 1, 1, '2021-04-13 12:57:28');

-- --------------------------------------------------------

--
-- Struktura tabulky `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tab` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `id_tab` int(11) DEFAULT NULL,
  `variable` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `data` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `insert_update` tinyint(1) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `tab` (`tab`),
  KEY `id_tab` (`id_tab`),
  KEY `variable` (`variable`),
  KEY `data` (`data`),
  KEY `time` (`time`),
  KEY `insert_update` (`insert_update`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `logs`
--

INSERT INTO `logs` (`id`, `tab`, `id_tab`, `variable`, `data`, `insert_update`, `time`) VALUES
(1, 'books', 6, 'name', 'čžščřž', 0, '2021-04-13 12:50:47'),
(2, 'books', 6, 'name', 'ÄÅ¾Å¡ÄÅ™Å¾', 0, '2021-04-13 12:50:59'),
(3, 'books', 6, 'author', 'Å¡ÄÅ™Å¾Å¡ÄÅ™Å¾', 0, '2021-04-13 12:50:59'),
(4, 'books', 6, 'isbn', 'Å¡ÄÅ™Å¾Å¡ÄÅ™Å¾', 0, '2021-04-13 12:50:59'),
(5, 'books', 6, 'warehouse', '1', 0, '2021-04-13 12:50:59'),
(6, 'books', 6, 'name', 'čžščřž', 0, '2021-04-13 12:51:49'),
(7, 'books', 6, 'name', 'ÄÅ¾Å¡ÄÅ™Å¾', 0, '2021-04-13 12:53:34'),
(8, 'books', 6, 'author', 'Å¡ÄÅ™Å¾Å¡ÄÅ™Å¾', 0, '2021-04-13 12:53:34'),
(9, 'books', 6, 'isbn', 'Å¡ÄÅ™Å¾Å¡ÄÅ™Å¾', 0, '2021-04-13 12:53:34'),
(10, 'books', 6, 'warehouse', '1', 0, '2021-04-13 12:53:34'),
(11, 'books', 6, 'name', 'Ä›Å¡ÄÅ¡ÄÅ¾', 0, '2021-04-13 12:53:48'),
(12, 'books', 6, 'author', 'Å¡ÄÅ™Å¾Å¡ÄÅ¾', 0, '2021-04-13 12:53:48'),
(13, 'books', 6, 'isbn', 'Å¡ÄÅ™Å¾Å¡ÄÅ™Å¾', 0, '2021-04-13 12:53:48'),
(14, 'books', 6, 'warehouse', '1', 0, '2021-04-13 12:53:48'),
(15, 'books', 6, 'name', 'Ä›Å¡ÄÅ¡ÄÅ¾', 0, '2021-04-13 12:56:34'),
(16, 'books', 6, 'author', 'Å¡ÄÅ™Å¾Å¡ÄÅ¾', 0, '2021-04-13 12:56:34'),
(17, 'books', 6, 'isbn', 'Å¡ÄÅ™Å¾Å¡ÄÅ™Å¾', 0, '2021-04-13 12:56:34'),
(18, 'books', 6, 'warehouse', '1', 0, '2021-04-13 12:56:34'),
(19, 'books', 6, 'name', 'ÄÅ¡Å¾Å™Å¡Å¾', 0, '2021-04-13 12:57:02'),
(20, 'books', 6, 'author', 'Å¡ÄÅ™Å¾Å¡ÄÅ™Å¾', 0, '2021-04-13 12:57:02'),
(21, 'books', 6, 'isbn', 'ÄÅ¡Å™Å¾Å¡ÄÅ¾', 0, '2021-04-13 12:57:02'),
(22, 'books', 6, 'warehouse', '1', 0, '2021-04-13 12:57:02'),
(23, 'books', 27, 'name', 'Å¡Å¾Å™Å¡Å¾Å¡Äfgsdfg', 1, '2021-04-13 12:57:20'),
(24, 'books', 27, 'author', 'Å¡ÄÅ™Å¾Å¡ÄÅ™Å¾Å¡', 1, '2021-04-13 12:57:20'),
(25, 'books', 27, 'isbn', 'Å¡ÄÅ™Å¡Å¾Å¡ÄÅ™Å¾', 1, '2021-04-13 12:57:20'),
(26, 'books', 27, 'warehouse', '0', 1, '2021-04-13 12:57:20'),
(27, 'lended_books', 6, 'id_books', '25', 0, '2021-04-13 12:57:28'),
(28, 'lended_books', 6, 'id_users', '27', 0, '2021-04-13 12:57:28'),
(29, 'lended_books', 6, 'date_lend', '2021-04-13', 0, '2021-04-13 12:57:28'),
(30, 'lended_books', 6, 'date_return', '2021-07-13', 0, '2021-04-13 12:57:28'),
(31, 'lended_books', 6, 'actual_date_return', '', 0, '2021-04-13 12:57:28'),
(32, 'lended_books', 6, 'number', '1', 0, '2021-04-13 12:57:28'),
(33, 'lended_books', 6, 'active', '1', 0, '2021-04-13 12:57:28'),
(34, 'books', 25, 'name', 'Ä›Å¡Å¡Å™Å¡Ä+Ä›Å™Å¾Ã­Å™Å¾Å¡', 1, '2021-04-13 12:57:28'),
(35, 'books', 25, 'author', 'Ä›Å¡ÄÅ™Ã½Å¡Ä›Ã­ÄÅ™', 1, '2021-04-13 12:57:28'),
(36, 'books', 25, 'isbn', '12445236', 1, '2021-04-13 12:57:28'),
(37, 'books', 25, 'warehouse', '2', 1, '2021-04-13 12:57:28'),
(38, 'notes', 6, 'id_lendedbooks', '6', 0, '2021-04-13 12:57:28'),
(39, 'notes', 6, 'note', 'sdfgsdfg', 0, '2021-04-13 12:57:28'),
(40, 'notes', 6, 'lend_return', '0', 0, '2021-04-13 12:57:28'),
(41, 'notes', 6, 'id_books', '25', 0, '2021-04-13 12:57:28'),
(42, 'notes', 6, 'id_users', '27', 0, '2021-04-13 12:57:28');

-- --------------------------------------------------------

--
-- Struktura tabulky `logs_login`
--

DROP TABLE IF EXISTS `logs_login`;
CREATE TABLE IF NOT EXISTS `logs_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) NOT NULL,
  `login_logout` tinyint(1) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `logs_login`
--

INSERT INTO `logs_login` (`id`, `id_users`, `login_logout`, `time`) VALUES
(1, 27, NULL, '2021-04-13 12:21:01'),
(2, 27, NULL, '2021-04-13 12:22:34'),
(3, 27, 0, '2021-04-13 13:00:50'),
(4, 27, 0, '2021-04-13 13:01:51'),
(5, 18, 0, '2021-04-13 13:02:44'),
(6, 18, 0, '2021-04-13 13:02:52'),
(7, 27, 0, '2021-04-13 13:02:59'),
(8, 27, 0, '2021-04-13 13:03:52'),
(9, 27, 0, '2021-04-13 13:03:56'),
(10, 27, 1, '2021-04-13 13:04:01');

-- --------------------------------------------------------

--
-- Struktura tabulky `notes`
--

DROP TABLE IF EXISTS `notes`;
CREATE TABLE IF NOT EXISTS `notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_lendedbooks` int(11) DEFAULT NULL,
  `id_books` int(11) DEFAULT NULL,
  `id_users` int(11) DEFAULT NULL,
  `note` varchar(2000) COLLATE utf8_czech_ci DEFAULT NULL,
  `lend_return` tinyint(1) DEFAULT NULL,
  `changes` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_pujceni` (`id_lendedbooks`),
  KEY `pujceni/vraceni` (`lend_return`),
  KEY `uprava` (`changes`),
  KEY `id_books` (`id_books`),
  KEY `id_users` (`id_users`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `notes`
--

INSERT INTO `notes` (`id`, `id_lendedbooks`, `id_books`, `id_users`, `note`, `lend_return`, `changes`) VALUES
(8, 4, 1, 27, 'Karelll', 0, '2021-04-11 21:33:22'),
(3, 12, 1, 24, 'karellll', 0, '2021-04-10 19:21:54'),
(4, 12, 1, 24, 'prooooc', 1, '2021-04-10 19:22:04'),
(6, 13, 1, 24, 'joooooo', 1, '2021-04-10 19:28:12'),
(9, 6, 25, 27, 'sdfgsdfg', 0, '2021-04-13 12:57:28');

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `changes` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `jmeno` (`name`),
  KEY `nickname` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`id`, `username`, `name`, `password`, `admin`, `changes`) VALUES
(27, 'admin', 'admin', '$2y$10$BwxfYQwpxljVewyBremKB.mD.qcMOUOViFgJiUOOQAFW8kPtrhLvq', 1, '2021-04-11 18:41:05'),
(28, 'prochazka', 'prochazka', '$2y$10$1kRMTMYUbuaZmIZiWbe2G.OiNy08FGrae/2y7vgBzFp9/Yhk59aHS', 0, '2021-04-11 18:44:35'),
(18, 'cernyf', 'Filip ÄŒernÃ½', '$2y$10$IzCEp/IgKUN3fL3D3Q55g.I3t8.JY/RhCmYtbkSb0TqLMDWTgRhcO', 0, '2021-04-11 18:44:41');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
