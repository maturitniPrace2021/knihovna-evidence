<?php
require "../vendor/autoload.php";
require_once("../include.php");
$latte = new Latte\Engine;


$latte->setTempDirectory('temp');

if (isset($_GET['vratit'])) {
  $vratit = true;
}
else {
  $vratit = false;
}

$obj_books = new Books();

//search
if (isset($_GET['search'])) {
  $unsafevariables[0] = "%".htmlspecialchars($_GET['search'])."%";
  $unsafevariables[1] = "%".htmlspecialchars($_GET['search'])."%";
  $unsafevariables[2] = "%".htmlspecialchars($_GET['search'])."%";
  $LendBooks = $obj_books->getFilterBooksby_usersafe(htmlspecialchars($_SESSION['user_id']),"AND (`name` LIKE ?) OR (`author` LIKE ?) OR (`isbn` LIKE ?)",'',$unsafevariables);
}
else {
  if (isset($_SESSION['user_id'])) {
    $LendBooks = $obj_books->getFilterBooksby_user(htmlspecialchars($_SESSION['user_id']));
  }
  else{
    $LendBooks[0]['name'] = "žádná data";
  }
}

//format date pro vypis
foreach ($LendBooks as $key => $value) {
  foreach ($value as $key2 => $value2) {
    if ($key2 == 10) {
      $LendBooks[$key][10] = date("d.m.Y", strtotime($value2));
    }
    if ($key2 == 9) {
      $LendBooks[$key][9] = date("d.m.Y", strtotime($value2));
    }
  }
}



if (isset($_SESSION['user_id'])) {
  //headertag
  $obj_lendedbooks = new Lended_books();
  $sql = "SELECT SUM(number) as sum_lendedbooks FROM lended_books WHERE id_users = '".htmlspecialchars($_SESSION['user_id'])."' AND active = 1";
  $header_tag = $obj_lendedbooks->getFilterLended_books_sql($sql);
  $_SESSION['header_tag'] = $header_tag[0]['sum_lendedbooks'];
}

//autocomplete
$listBooks_autocomplete = $obj_books->getFilterBooksby_user(htmlspecialchars($_SESSION['user_id']));
foreach ($listBooks_autocomplete as $key => $value) {
  foreach ($value as $key2 => $value2) {
    if ($key2 == "name" OR $key2 == "author" OR $key2 == "isbn") {
      $autocomplete[] = $value2;
    }
  }
}
$_SESSION['autocomplete'] = $autocomplete;


$params = ['filepath' => $filepath,'LendBooks' => $LendBooks, 'vratit' => $vratit
];

// kresli na výstup
$latte->render('../template/Index/Zapujceneknihy.latte', $params);

?>
