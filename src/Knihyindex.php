<?php
require "../vendor/autoload.php";
require_once("../include.php");

$latte = new Latte\Engine;


$latte->setTempDirectory('temp');

$obj_books = new Books();


//search
if (isset($_GET['search'])) {
  $unsafevariables[0] = "%".htmlspecialchars($_GET['search'])."%";
  $unsafevariables[1] = "%".htmlspecialchars($_GET['search'])."%";
  $unsafevariables[2] = "%".htmlspecialchars($_GET['search'])."%";
  $listBooks = $obj_books->getFilterBooks_sqlWHEREsafe("(`name` LIKE ?) OR (`author` LIKE ?) OR (`isbn` LIKE ?)",$unsafevariables);
}
else {
  $listBooks = $obj_books->getAllBooks();
}


$pujcit = false;
if (isset($_GET['odebrat'])) {
  $odebrat = true;
}
else {
  $odebrat = false;
}

//autocomplete
$listBooks_autocomplete = $obj_books->getAllBooks();
foreach ($listBooks_autocomplete as $key => $value) {
  foreach ($value as $key2 => $value2) {
    if ($key2 == "name" OR $key2 == "author" OR $key2 == "isbn") {
      $autocomplete[] = $value2;
    }
  }
}
$_SESSION['autocomplete'] = $autocomplete;


$params = ['filepath' => $filepath,'listBooks' => $listBooks, 'pujcit' => $pujcit, 'odebrat' => $odebrat
];

// kresli na výstup
$latte->render('../template/Index/Knihyindex.latte', $params);

?>
