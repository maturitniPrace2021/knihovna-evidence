<?php
  if(session_id() == ''){session_start();} if (isset($_SESSION['admin']) AND $_SESSION['admin'] == 1) {

    require "../../vendor/autoload.php";
    require_once("../../include.php");

    $latte = new Latte\Engine;

    $latte->setTempDirectory('../temp');

    $params = ['filepath' => $filepath
    ];

    // kresli na výstup
    $latte->render('../../template/Index/Adduserdetail.latte', $params);
  }
  else{
    echo '<script>alert("Nemáte oprávnění")</script>';
  }

?>
