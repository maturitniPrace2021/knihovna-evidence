<?php
require "../../vendor/autoload.php";
require_once("../../include.php");

$latte = new Latte\Engine;


$latte->setTempDirectory('../temp');

$id_books = $_GET['id'];
$obj_books = new Books();
$detailBook = $obj_books->detailBookby_Id($id_books);

$obj_notes = new Notes();
$lendNotes = $obj_notes->getLendNotesby_idbooks($id_books);
$returnNotes = $obj_notes->getReturnNotesby_idbooks($id_books);

if (isset($_GET['knihykdispozici'])) {
  $knihykdispozici = true;
}
else {
  $knihykdispozici = false;
}

$params = ['filepath' => $filepath,'detailBook' => $detailBook, 'lendNotes' => $lendNotes, 'returnNotes' => $returnNotes, 'id_books' => $id_books, 'knihykdispozici' => $knihykdispozici
];

// kresli na výstup
$latte->render('../../template/Index/Knihadetail.latte', $params);

?>
