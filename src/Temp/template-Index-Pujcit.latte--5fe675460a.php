<?php
// source: ../../template/Index/Pujcit.latte

use Latte\Runtime as LR;

final class Template5fe675460a extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../frontend/dist/style_less.css">

    <title>Pujčit si Knihu</title>
</head>
<body>
    <header>
';
		/* line 15 */
		$this->createTemplate('../Include/Header/header.latte', $this->params, 'include')->renderToContentType('html');
		echo '    </header>

';
		if (isset($_GET['error'])) {
			/* line 19 */
			$this->createTemplate('../Include/Error/error.latte', $this->params, 'include')->renderToContentType('html');
		}
		echo '
    <nav>
';
		/* line 23 */
		$this->createTemplate('../Include/Nav/nav.latte', $this->params, 'include')->renderToContentType('html');
		echo '    </nav>

    <main>
';
		/* line 27 */
		$this->createTemplate('../Include/Main/pujcit.latte', $this->params, 'include')->renderToContentType('html');
		echo '    </main>

    <footer>
';
		/* line 31 */
		$this->createTemplate('../Include/Footer/footer.latte', $this->params, 'include')->renderToContentType('html');
		echo '    </footer>

';
		/* line 34 */
		$this->createTemplate('../Script/script.latte', $this->params, 'include')->renderToContentType('html');
		echo '</body>
</html>
';
		return get_defined_vars();
	}

}
