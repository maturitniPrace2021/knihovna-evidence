<?php
// source: ../template/Index/Knihyindex.latte

use Latte\Runtime as LR;

final class Template9d87a80ed8 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Knihovna knih">
    <meta name="keywords" content="HTML, CSS, JavaScript, Latte, Less, PHP, MySql">
    <meta name="author" content="Filip Černý, Tobiáš Procházka">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../frontend/dist/style_less.css">

    <title>';
		if ($pujcit == "true") {
			echo '
        Půjčit si knihu
';
		}
		elseif ($odebrat == "true") {
			echo '        Odebrat knihu
';
		}
		elseif (isset($_GET['knihykdispozici']) AND $_GET['knihykdispozici'] == "true") {
			echo '        Knihy k dispozici
';
		}
		elseif (isset($_GET['pujcitknihuzanekoho']) AND $_GET['pujcitknihuzanekoho'] == "true") {
			echo '        Půjčit knihu za někoho
';
		}
		elseif (isset($_GET['editovatknihu']) AND $_GET['editovatknihu'] == "true") {
			echo '        Editovat knihu
';
		}
		else {
			echo '        ';
		}
		echo '</title>
</head>
<body>
    <header>
';
		/* line 29 */
		$this->createTemplate('../Include/Header/header.latte', $this->params, 'include')->renderToContentType('html');
		echo '    </header>

';
		if (isset($_GET['error'])) {
			/* line 33 */
			$this->createTemplate('../Include/Error/error.latte', $this->params, 'include')->renderToContentType('html');
		}
		echo '
    <nav>
';
		/* line 37 */
		$this->createTemplate('../Include/Nav/nav.latte', $this->params, 'include')->renderToContentType('html');
		echo '    </nav>

';
		if (isset($_SESSION['admin']) AND $_SESSION['admin'] == 1 AND !isset($_GET['knihykdispozici']) AND !isset($_GET['pujcitknihuzanekoho']) AND !isset($_GET['odebrat']) AND !isset($_GET['pujcit'])) {
			echo '    <nav class="secoundnav">
';
			/* line 42 */
			$this->createTemplate('../Include/Main/addbook.latte', $this->params, 'include')->renderToContentType('html');
			echo '    </nav>
';
		}
		echo '
    <main>

';
		if (!isset($_SESSION['user_id']) AND (isset($_GET['pujcit']) AND $_GET['pujcit'] =='true')) {
			/* line 49 */
			$this->createTemplate('../Include/Main/prihlastese.latte', $this->params, 'include')->renderToContentType('html');
		}
		else {
			echo "\n";
			if ($pujcit == "true") {
				echo '        <h2 style="padding-bottom: 50px; color: white;">Půjčit si knihu</h2>
';
			}
			elseif ($odebrat == "true") {
				echo '        <h2 style="padding-bottom: 50px; color: white;">Odebrat knihu</h2>
';
			}
			elseif (isset($_GET['knihykdispozici']) AND $_GET['knihykdispozici'] == "true") {
				echo '        <h2 style="padding-bottom: 50px; color: white;">Knihy k dispozici</h2>
';
			}
			elseif (isset($_GET['pujcitknihuzanekoho']) AND $_GET['pujcitknihuzanekoho'] == "true") {
				echo '        <h2 style="padding-bottom: 50px; color: white;">Půjčit knihu za někoho</h2>
';
			}
			elseif (isset($_GET['editovatknihu']) AND $_GET['editovatknihu'] == "true") {
				echo '        <h2 style="padding-bottom: 50px; color: white;">Editovat knihu</h2>
';
			}
			else {
			}
			/* line 64 */
			$this->createTemplate('../Include/Main/mainknihy.latte', $this->params, 'include')->renderToContentType('html');
		}
		echo '
    </main>

    <footer>
';
		/* line 70 */
		$this->createTemplate('../Include/Footer/footer.latte', $this->params, 'include')->renderToContentType('html');
		echo '    </footer>
';
		/* line 72 */
		$this->createTemplate('../Script/script.latte', $this->params, 'include')->renderToContentType('html');
		echo '</body>
</html>
';
		return get_defined_vars();
	}

}
