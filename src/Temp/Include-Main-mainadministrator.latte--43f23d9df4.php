<?php
// source: template\Include\Main\mainadministrator.latte

use Latte\Runtime as LR;

final class Template43f23d9df4 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		if (isset($_SESSION['admin']) AND $_SESSION['admin'] == 1) {
			echo '<section class="home">
    <a href="src/Knihykdispozici.php?knihykdispozici=true">
        <article>
            <h2>Knihy k dispozici</h2>
            <div class="center">
                <i class=" fa fa-book"></i>
            </div>
        </article>
    </a>
    <a href="src/Zapujceneknihy.php">
        <article class="center">
            <h2>Zapůjčené knihy</h2>
            <div class="center">
';
			if (isset($_SESSION['header_tag']) AND isset($_SESSION['username'])) {
				$header_tag = $_SESSION['header_tag'];
			}
			else {
				$header_tag = "0";
			}
			echo '                <p>Máš půjčeno:<br> ';
			echo LR\Filters::escapeHtmlText($header_tag) /* line 20 */;
			if ($header_tag == 1) {
				echo ' Knihu ';
			}
			elseif ($header_tag == 2 || $header_tag == 3 || $header_tag == 4) {
				echo ' Knihy ';
			}
			else {
				echo ' Knih';
			}
			echo ' </p>
            </div>
        </article>
    </a>

    <a href="src/Knihykdispozici.php?pujcit=true">
        <article class="center">
            <h2>Půjčit si knihu</h2>
            <div class="center">
                <i class=" fa fa-plus"></i>
            </div>
        </article>
    </a>
    <a href="src/Zapujceneknihy.php?vratit=true">
        <article class="center">
            <h2>Vrátit knihu</h2>
            <div class="center">
                <i class=" fa fa-minus"></i>
            </div>
        </article>
    </a>
    <a href="src/Detail/Pridatknihuindex.php">
        <article class="center">
            <h2>Přidat knihu</h2>
            <div class="center">
                <i class=" fa fa-plus"></i>
            </div>
        </article>
    </a>
    <a href="src/Knihyindex.php?odebrat=true">
        <article class="center">
            <h2>Odebrat knihu</h2>
            <div class="center">
                <i class=" fa fa-minus"></i>
            </div>
        </article>
    </a>

    <a href="src/Knihykdispozici.php?pujcitknihuzanekoho=true">
        <article class="center">
            <h2>Půjčit knihu za někoho</h2>
            <div class="center">
                <i class=" fa fa-plus"></i>
            </div>
        </article>
    </a>
    <a href="src/Zapujceneknihyall.php?vratitall=true">
        <article class="center">
            <h2>Vrátit knihu za někoho</h2>
            <div class="center">
                <i class=" fa fa-minus"></i>
            </div>
        </article>
    </a>
    <a href="src/Knihyindex.php?editovatknihu=true">
        <article class="center">
            <h2>Editovat knihu</h2>
            <div class="center">
                <i class="fa fa-book"></i>
            </div>
        </article>
    </a>
    <a href="src/Users.php">
        <article class="center">
            <h2>Uživatelé</h2>
            <div class="center">
                <i class="fa fa-user"></i>
            </div>
        </article>
    </a>
    <a href="src/Rad.php?rad=true">
        <article class="center">
            <h2>Editovat řád</h2>
            <div class="center">
                <i class="fa fa-edit"></i>
            </div>
        </article>
    </a>
</section>
';
		}
		else {
			echo ' <script>alert("Nemáte oprávnění")</script>;
';
		}
		return get_defined_vars();
	}

}
