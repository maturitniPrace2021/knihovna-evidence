<?php
// source: ../../template/Include/Error/error.latte

use Latte\Runtime as LR;

final class Template8acfacb1e8 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		$url = $_SESSION['errorurl'];
		echo '<div class="errorblur">
    <div class="errormenu">
        <div class="errorheader">
            <h2>Upozornění!</h2>
            <div class="close">
';
		if (isset($_GET['deleteuser'])) {
			echo '                <a href=';
			echo LR\Filters::escapeHtmlAttrUnquoted(LR\Filters::safeUrl($filepath . 'src/Users.php')) /* line 8 */;
			echo ' class="fa fa-times"></a>
';
		}
		elseif (isset($_GET['deletenote'])) {
			echo '                <a href=';
			echo LR\Filters::escapeHtmlAttrUnquoted(LR\Filters::safeUrl($filepath . 'src/Detail/Editbook.php?id=' . $_GET['id'])) /* line 10 */;
			echo ' class="fa fa-times"></a>
';
		}
		else {
			echo '                <a href=';
			echo LR\Filters::escapeHtmlAttrUnquoted(LR\Filters::safeUrl($filepath . $url)) /* line 12 */;
			echo ' class="fa fa-times"></a>
';
		}
		echo '            </div>
        </div>

        <div class="errortag">
            <p>';
		echo LR\Filters::escapeHtmlText($_GET['error']) /* line 18 */;
		echo '</p>
        </div>

        <div class="button">
';
		if (isset($_GET['deleteuser'])) {
			echo '        <button onclick= "location.href=';
			echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($filepath . $url)) /* line 23 */;
			echo '">Smazat</button>
';
		}
		elseif (isset($_GET['deletenote'])) {
			echo '        <button onclick= "location.href=';
			echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($filepath . $url)) /* line 25 */;
			echo '">Smazat</button>
';
		}
		echo '        </div>
    </div>

</div>
';
		return get_defined_vars();
	}

}
