<?php
// source: ..\..\template\Include\Main\maindetail.latte

use Latte\Runtime as LR;

final class Template7a9ad1535a extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<section class="detail">
';
		$returnurl = './Vratit.php?id=';
		$lendurl = './Pujcit.php?id=';
		$editurl = './Editbook.php?id=';
		$deleteurl = './Odebratknihu.php?id=';
		echo '

';
		$iterations = 0;
		foreach ($detailBook as $value => $key) {
			$ʟ_switch = ($value);
			if (false) {
			}
			elseif (in_array($ʟ_switch, ['name'], true)) {
				$name_book = $key;
			}
			elseif (in_array($ʟ_switch, ['warehouse'], true)) {
				$warehouse = $key;
			}
			elseif (in_array($ʟ_switch, ['author'], true)) {
				$author = $key;
			}
			elseif (in_array($ʟ_switch, ['isbn'], true)) {
				$isbn = $key;
			}
			$iterations++;
		}
		echo '


';
		$iterations = 0;
		foreach ($returnNotes as $rNotes) {
			$iterations = 0;
			foreach ($rNotes as $value => $key) {
				$ʟ_switch = ($value);
				if (false) {
				}
				elseif (in_array($ʟ_switch, ['note'], true)) {
					$returnNoteskey = $key;
				}
				elseif (in_array($ʟ_switch, ['changes'], true)) {
					$returnNoteschangeskey = $key;
				}
				elseif (in_array($ʟ_switch, ['id_users'], true)) {
					$returnNotesnamekey = $key;
				}
				$iterations++;
			}
			$iterations++;
		}
		echo '

    <div class="whatisit">
        <h2>Kniha</h2>
        <button onclick="goBack()">Zpátky</button>

    </div>
    <article>
        <div class="form">
            <div class="first">
                <div class="div">
                    <div class="label">
                        <label>Název Knihy:</label>
                    </div>
                    <div class="input_content_cantfill">
                        <p>';
		echo LR\Filters::escapeHtmlText($name_book) /* line 50 */;
		echo '</p>
                    </div>
                </div>
                <div class="div">
                    <div class="label">
                        <label>Název autora:</label>
                    </div>
                    <div class="input_content_cantfill">
                        <p>';
		echo LR\Filters::escapeHtmlText($author) /* line 58 */;
		echo '</p>
                    </div>
                </div>
                <div class="div">
                    <div class="label">
                        <label>ISBN:</label>
                    </div>
                    <div class="input_content_cantfill">
                        <textarea disabled wrap="off">';
		echo LR\Filters::escapeHtmlText($isbn) /* line 66 */;
		echo '</textarea>
                    </div>
                </div>
                <div class="div">
                    <div class="label">
                        <label>Počet kusů na skladě:</label>
                    </div>
                    <div class="input_content_cantfill">
                        <input disabled type="number" min="0" max="number" id="warehouse" name="warehouse" value="';
		echo LR\Filters::escapeHtmlAttr($warehouse) /* line 74 */;
		echo '">
                    </div>
                </div>
            </div>

            <div class="secound">
                <div class="textarea">
                    <div class="label">
                        <label>Poznámky k vypůjčení</label>
                    </div>

                    <textarea disabled>
';
		$iterations = 0;
		foreach ($lendNotes as $lNotes) {
			$iterations = 0;
			foreach ($lNotes as $value => $key) {
				$ʟ_switch = ($value);
				if (false) {
				}
				elseif (in_array($ʟ_switch, ['username'], true)) {
					echo 'Přidáno uživatelem: ';
					echo LR\Filters::escapeHtmlText($key) /* line 90 */;
					echo '
_______________________

';
				}
				elseif (in_array($ʟ_switch, ['note'], true)) {
					echo 'Poznámka:
';
					echo LR\Filters::escapeHtmlText($key) /* line 95 */;
					echo '

';
				}
				elseif (in_array($ʟ_switch, ['changes'], true)) {
					echo 'Přidáno dne: ';
					echo LR\Filters::escapeHtmlText($key) /* line 98 */;
					echo "\n";
				}
				$iterations++;
			}
			$iterations++;
		}
		echo '</textarea>
                </div>
            </div>
            <div class="third">

                <div class="textarea">
                    <div class="label">
                        <label>Poznámky k vrácení</label>
                    </div>
                    <textarea disabled>
';
		$iterations = 0;
		foreach ($returnNotes as $rNotes) {
			$iterations = 0;
			foreach ($rNotes as $value => $key) {
				$ʟ_switch = ($value);
				if (false) {
				}
				elseif (in_array($ʟ_switch, ['username'], true)) {
					echo 'Přidáno uživatelem: ';
					echo LR\Filters::escapeHtmlText($key) /* line 116 */;
					echo '
_______________________

';
				}
				elseif (in_array($ʟ_switch, ['note'], true)) {
					echo 'Poznámka:
';
					echo LR\Filters::escapeHtmlText($key) /* line 121 */;
					echo '

';
				}
				elseif (in_array($ʟ_switch, ['changes'], true)) {
					echo 'Přidáno dne: ';
					echo LR\Filters::escapeHtmlText($key) /* line 124 */;
					echo "\n";
				}
				$iterations++;
			}
			$iterations++;
		}
		echo '</textarea>
                </div>
            </div>
            <div class="four">
';
		if ($knihykdispozici == "true") {
			echo '                <div class="button">
                    <button onclick= "location.href=';
			echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($lendurl . $id_books)) /* line 134 */;
			echo '">Půčit si</button>
                </div>
';
		}
		else {
			echo '                <div class="button">
                    <button onclick= "location.href=';
			echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($lendurl . $id_books)) /* line 138 */;
			echo '">Půčit si</button>
                </div>
';
		}
		echo "\n";
		if (isset($_SESSION['admin']) AND $_SESSION['admin'] == 1) {
			echo '                <div class="button">
                    <button onclick= "location.href=';
			echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($editurl . $id_books)) /* line 144 */;
			echo '">Editovat</button>
                </div>
';
		}
		if (isset($_SESSION['admin']) AND $_SESSION['admin'] == 1) {
			echo '                <div class="button">
                    <button onclick= "location.href=';
			echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($deleteurl . $id_books)) /* line 149 */;
			echo '">Odebrat</button>
                </div>
';
		}
		echo '            </div>
        </div>
    </article>

</section>
';
		return get_defined_vars();
	}


	public function prepare(): void
	{
		extract($this->params);
		if (!$this->getReferringTemplate() || $this->getReferenceType() === "extends") {
			foreach (array_intersect_key(['value' => '8, 24, 87, 113', 'key' => '8, 24, 87, 113', 'rNotes' => '23, 112', 'lNotes' => '86'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		
	}

}
