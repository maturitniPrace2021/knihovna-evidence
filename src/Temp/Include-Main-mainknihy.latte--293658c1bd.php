<?php
// source: ../template/Include/Main/mainknihy.latte

use Latte\Runtime as LR;

final class Template293658c1bd extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<section class="knihy">
';
		$url = './Detail/Knihadetail.php?id=';
		$url2 = './Detail/Pujcit.php?id=';
		$url3 = 'Detail/Odebratknihu.php?id=';
		$url4 = './Detail/Knihadetail.php?knihykdispozici=true&id=';
		$url5 = 'Detail/Pujcitknihuzanekoho.php?id=';
		$url6 = './Detail/Editbook.php?id=';
		echo '


';
		$iterations = 0;
		foreach ($listBooks as $Books) {
			$iterations = 0;
			foreach ($Books as $value => $key) {
				$ʟ_switch = ($value);
				if (false) {
				}
				elseif (in_array($ʟ_switch, ['id'], true)) {
					if ($pujcit == "true") {
						echo '              <a href=';
						echo LR\Filters::escapeHtmlAttrUnquoted(LR\Filters::safeUrl($url2 . $key)) /* line 16 */;
						echo '>
';
					}
					elseif ($odebrat == "true") {
						echo '              <a href=';
						echo LR\Filters::escapeHtmlAttrUnquoted(LR\Filters::safeUrl($url3 . $key)) /* line 18 */;
						echo '>
';
					}
					elseif (isset($_GET['knihykdispozici']) AND $_GET['knihykdispozici'] == "true") {
						echo '              <a href=';
						echo LR\Filters::escapeHtmlAttrUnquoted(LR\Filters::safeUrl($url4 . $key)) /* line 20 */;
						echo '>
';
					}
					elseif (isset($_GET['pujcitknihuzanekoho']) AND $_GET['pujcitknihuzanekoho'] == "true") {
						echo '              <a href=';
						echo LR\Filters::escapeHtmlAttrUnquoted(LR\Filters::safeUrl($url5 . $key)) /* line 22 */;
						echo '>
';
					}
					elseif (isset($_GET['editovatknihu']) AND $_GET['editovatknihu'] == "true") {
						echo '              <a href=';
						echo LR\Filters::escapeHtmlAttrUnquoted(LR\Filters::safeUrl($url6 . $key)) /* line 24 */;
						echo '>
';
					}
					else {
						echo '              <a href=';
						echo LR\Filters::escapeHtmlAttrUnquoted(LR\Filters::safeUrl($url . $key)) /* line 26 */;
						echo '>
';
					}
					echo "\n";
				}
				elseif (in_array($ʟ_switch, ['name'], true)) {
					echo '          <article>
            <h2>';
					echo LR\Filters::escapeHtmlText(($this->filters->truncate)(($this->filters->breaklines)($key), 30)) /* line 31 */;
					echo '</h2>
';
				}
				elseif (in_array($ʟ_switch, ['author'], true)) {
					echo '            <div>
            <p>';
					echo LR\Filters::escapeHtmlText($key) /* line 34 */;
					echo '</p>
';
				}
				elseif (in_array($ʟ_switch, ['warehouse'], true)) {
					echo '            <p>Počet skladem:';
					echo LR\Filters::escapeHtmlText($key) /* line 36 */;
					echo '</p>
            </div>
          </article>

';
				}
				$iterations++;
			}
			echo "\n";
			$iterations++;
		}
		echo '  </a>

</section>
';
		return get_defined_vars();
	}


	public function prepare(): void
	{
		extract($this->params);
		if (!$this->getReferringTemplate() || $this->getReferenceType() === "extends") {
			foreach (array_intersect_key(['value' => '12', 'key' => '12', 'Books' => '11'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		
	}

}
