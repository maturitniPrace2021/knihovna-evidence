<?php
// source: ../template/Include/Main/main.latte

use Latte\Runtime as LR;

final class Template86045c020b extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<section class="home">
    <a href="Knihykdispozici.php?knihykdispozici=true">
        <article>
            <h2>Knihy k dispozici</h2>
            <div class="center">
                <i class=" fa fa-book"></i>
            </div>
        </article>
    </a>
    <a href="Zapujceneknihy.php">
        <article>
            <h2>Zapůjčené knihy</h2>
            <div class="center">
';
		if (isset($_SESSION['header_tag']) AND isset($_SESSION['username'])) {
			$header_tag = $_SESSION['header_tag'];
		}
		else {
			$header_tag = "0";
		}
		echo '                <p>Máš půjčeno:<br> ';
		echo LR\Filters::escapeHtmlText($header_tag) /* line 19 */;
		if ($header_tag == 1) {
			echo ' Knihu ';
		}
		elseif ($header_tag == 2 || $header_tag == 3 || $header_tag == 4) {
			echo ' Knihy ';
		}
		else {
			echo ' Knih';
		}
		echo ' </p>
            </div>
        </article>
    </a>

    <a href="Knihykdispozici.php?pujcit=true">
        <article>
            <h2>Půjčit si knihu</h2>
            <div class="center">
                <i class=" fa fa-plus"></i>
            </div>
        </article>
    </a>
    <a href="Zapujceneknihy.php?vratit=true">
        <article>
            <h2>Vrátit knihu</h2>
            <div class="center">
                <i class=" fa fa-minus"></i>
            </div>
        </article>
    </a>
</section>
';
		return get_defined_vars();
	}

}
