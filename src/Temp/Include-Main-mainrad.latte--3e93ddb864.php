<?php
// source: ../template/Include/Main/mainrad.latte

use Latte\Runtime as LR;

final class Template3e93ddb864 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<section class="rad">
    <div class="whatisit">
        <h2>Řád</h2>
        <button onclick="goBack()">Zpátky</button>

    </div>
    <article>
        <div class="form">
            <div class="first">
                <div class="input_content">
                    <p>';
		echo LR\Filters::escapeHtmlText($radtext) /* line 11 */;
		echo '</p>
                </div>
            </div>
            <div class="secound">
';
		if (isset($_SESSION['admin']) AND $_SESSION['admin'] == 1) {
			echo '                <div class="button">
                    <button onclick="location.href=\'Rad.php?rad=true\'">Editovat</button>
                </div>
';
		}
		echo '            </div>
        </div>
    </article>
</section>
';
		return get_defined_vars();
	}

}
