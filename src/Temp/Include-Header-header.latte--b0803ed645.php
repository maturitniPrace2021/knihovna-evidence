<?php
// source: ..\..\template\Include\Header\header.latte

use Latte\Runtime as LR;

final class Templateb0803ed645 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '
<div class="header">
    <div class="Logo">
        <h1><a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($filepath)) /* line 4 */;
		echo 'Index.php"><span class="fa fa-book"></span>Knihovna</a></h1>
    </div>
    <ul class="Header_cantainer">
';
		if (isset($_SESSION['username'])) {
			$username = $_SESSION['username'];
			$url = "{$filepath}src/Detail/Logout.php";
		}
		else {
			$username = "Přihlásit se";
			$url = "{$filepath}src/Detail/Login.php";
		}
		echo '        <a href=';
		echo LR\Filters::escapeHtmlAttrUnquoted(LR\Filters::safeUrl($url)) /* line 14 */;
		echo '><li><h1>';
		echo LR\Filters::escapeHtmlText($username) /* line 14 */;
		echo '</h1></li></a>
';
		if (isset($_SESSION['header_tag']) AND isset($_SESSION['username'])) {
			$header_tag = $_SESSION['header_tag'];
		}
		else {
			$header_tag = "0";
		}
		echo '        <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($filepath)) /* line 20 */;
		echo 'src/Zapujceneknihy.php"><li><h1><span class="fa fa-book"><span>';
		echo LR\Filters::escapeHtmlText($header_tag) /* line 20 */;
		echo '</span></span><h1></li></a>
    </ul>
</div>
';
		return get_defined_vars();
	}

}
