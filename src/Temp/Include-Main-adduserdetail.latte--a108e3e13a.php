<?php
// source: ../../template/Include/Main/adduserdetail.latte

use Latte\Runtime as LR;

final class Templatea108e3e13a extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		if (isset($_SESSION['admin']) AND $_SESSION['admin'] == 1) {
			echo '<section class="adduserdetail">
    <div class="whatisit">
        <div><h2>Přidat uživatele</h2></div>
        <button onclick="goBack()">Zpátky</button>

    </div>
    <article>
        <form id="adduser" action="../Formaction/Adduserindex.php" method="post">
            <div class="first">
                <div class="div">
                    <div class="label">
                        <label for="name">Jméno uživatele:</label>
                    </div>
                    <div class="input_content_canfill">
                        <input type="text"id="username" name="name" required>
                    </div>
                </div>

                <div class="div">
                    <div class="label">
                        <label for="username">Uživatelské jméno:</label>
                    </div>
                    <div class="input_content_canfill">
                        <input type="text"id="username" name="username" required>
                    </div>
                </div>

                <div class="div">
                    <div class="label">
                        <label for="admin">Vyberte oprávnění:</label>
                    </div>
                    <div class="input_content_canfill">
                        <select name = "admin" required="required">
                        <option value=\'0\' selected>uživatel</option>
                        <option value=\'1\'>admin</option>
                        </select>
                    </div>
                </div>

                <div class="div">
                    <div class="label">
                        <label for="password">Heslo:</label>
                    </div>
                    <div class="input_content_canfill">
                        <input type="password"id="password" name="password" required>
                    </div>
                </div>

                <div class="div">
                    <div class="label">
                        <label for="password1">Heslo znovu:</label>
                    </div>
                    <div class="input_content_canfill">
                        <input type="password"id="password" name="password1" required>
                    </div>
                </div>

            </div>
            <div class="secound">
                <div class="button">
                    <button type="submit" form="adduser">Přidat</button>
                </div>
            </div>
        </form>
    </article>
</section>
';
		}
		else {
			echo '<script>alert("Nemáte oprávnění")</script>
';
		}
		return get_defined_vars();
	}

}
