<?php
// source: ../template/Include/Footer/footer.latte

use Latte\Runtime as LR;

final class Templateb391fef43b extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<div class="footer">
    <aside class="first">
        <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($filepath)) /* line 3 */;
		echo 'Index.php">Home</a>
        <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($filepath)) /* line 4 */;
		echo 'src/Rad.php">Řád</a>
        <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($filepath)) /* line 5 */;
		echo 'src/Knihyindex.php">Knihy</a>
    </aside>
    <aside class="first">
        <h3>Kontakty:</h3>
        <li><a href="mailto:#"><p><span class="fa fa-envelope"></span> Knihovna@email.cz</p></a></li>
        <li><a href="tel:#"><p><span class="fa fa-phone"></span> 000 000 000</p></a></li>
    </aside>
    <aside class="aside_container">
';
		if (isset($_SESSION['user_id'])) {
			echo '        <a href="';
			echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($filepath)) /* line 14 */;
			echo 'src/Formaction/Logoutindex.php"><h3><span class="fa fa-sign-out"></span> Odhlášení<h3></a>
';
		}
		echo '    </aside>
</div>
';
		return get_defined_vars();
	}

}
