<?php
// source: ../../template/Include/Main/pujcit.latte

use Latte\Runtime as LR;

final class Templatec1211a9d8b extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<section class="lend">
    <div class="whatisit">
        <div><h2>Půjčit si knihu</h2></div>
        <button onclick="goBack()">Zpátky</button>

    </div>
    <article>
        <form id="LEND" action="../Formaction/Pujcitindex.php" method="post">
            <div class="first">
                <div class="div">
                    <div class="label">
                        <label>Název Knihy:</label>
                    </div>
                    <div class="input_content_cantfill">
                        <p>';
		echo LR\Filters::escapeHtmlText($name_book) /* line 15 */;
		echo '</p>
                    </div>
                </div>
                <div class="div">
                    <div class="label">
                        <label>Autor:</label>
                    </div>
                    <div class="input_content_cantfill">
                        <p>';
		echo LR\Filters::escapeHtmlText($author) /* line 23 */;
		echo '</p>
                    </div>
                </div>

                <div class="div">
                    <div class="label">
                        <label for="date_lend">Pujčit si od:</label>
                    </div>
                    <div class="input_content_canfill">
                        <input type="date" id="date_lend" name="date_lend" value="';
		echo LR\Filters::escapeHtmlAttr(date('Y-m-d')) /* line 32 */;
		echo '" required>
                    </div>
                </div>

                <div class="div">
                    <div class="label">
                        <label for="date_return">Pujčit si do:</label>
                    </div>
                    <div class="input_content_canfill">
                        <input type="date" id="date_return" name="date_return" value="';
		echo LR\Filters::escapeHtmlAttr(date('Y-m-d', strtotime('+3 month'))) /* line 41 */;
		echo '" required>
                    </div>
                </div>

                <div class="div">
                    <div class="label">
                        <label for="number">Počet kusů:</label>
                    </div>
                    <div class="input_content_canfill">
                        <input type="number" min="1" max=';
		echo LR\Filters::escapeHtmlAttrUnquoted($warehouse) /* line 50 */;
		echo ' id="number" name="number" value="1">
                    </div>
                </div>


            </div>

            <div class="secound">
                <div class="textarea">
                    <div class="label">
                        <label>Poznámka k vypůjčení</label>
                    </div>
                    <textarea id="note" name="note" placeholder="Poznámka..."></textarea>
                </div>
            </div>
            <div class="third">
                <input type="hidden" value=';
		echo LR\Filters::escapeHtmlAttrUnquoted($id_books) /* line 66 */;
		echo ' name= "id_books">
                <div class="button">
                    <button type="submit" form="LEND">Půjčit</button>
                </div>
            </div>
        </form>
    </article>
</section>
';
		return get_defined_vars();
	}

}
