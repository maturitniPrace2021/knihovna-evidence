<?php
// source: ../../template/Include/Header/headerlogin.latte

use Latte\Runtime as LR;

final class Templatef70f69849f extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<div class="header">
    <div class="Logo">
        <h1><a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($filepath)) /* line 3 */;
		echo 'Index.php"><span class="fa fa-book"></span>Knihovna</a></h1>
    </div>
</div>
';
		return get_defined_vars();
	}

}
