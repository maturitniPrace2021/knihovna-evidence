<?php
// source: ../template/Include/Main/editrad.latte

use Latte\Runtime as LR;

final class Templated2b94c2a79 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		if (isset($_SESSION['admin']) AND $_SESSION['admin'] == 1) {
			echo '<section class="editrad">
    <div class="whatisit">
        <h2>Edit řád</h2>
        <button onclick="goBack()">Zpátky</button>

    </div>
    <article>
        <form  id="rad" action="Formaction/Radindex.php" method= "POST">
            <div class="first">
                <div class="div">
                    <div class="input_content">
                        <textarea id="text" name="text" placeholder="Řád knihovny...">';
			echo LR\Filters::escapeHtmlText($radtext) /* line 13 */;
			echo '</textarea>
                    </div>
                </div>
            </div>
            <div class="secound">
                <div class="button">
                    <button type="submit" form="rad">Uložit</button>
                </div>
            </div>
        </form>
    </article>
</section>
';
		}
		else {
			echo '<script>alert("Nemáte oprávnění")</script>
';
		}
		return get_defined_vars();
	}

}
