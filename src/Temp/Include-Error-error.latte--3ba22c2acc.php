<?php
// source: ../template/Include/Error/error.latte

use Latte\Runtime as LR;

final class Template3ba22c2acc extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		$url = $_SESSION['errorurl'];
		echo '<div class="errorblur">
    <div class="errormenu">
        <div class="errorheader">
            <h2>Upozornění!</h2>
            <div class="close">
                <a href=';
		echo LR\Filters::escapeHtmlAttrUnquoted(LR\Filters::safeUrl($filepath . $url)) /* line 7 */;
		echo ' class="fa fa-times"></a>
            </div>
        </div>

        <div class="errortag">
            <p>';
		echo LR\Filters::escapeHtmlText($_GET['error']) /* line 12 */;
		echo '</p>
        </div>

        <div class="button">
';
		if (isset($_GET['deleteuser'])) {
			echo '        <button onclick= "location.href=';
			echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($filepath . $url)) /* line 17 */;
			echo '">Smazat</button>
        <button onclick= "location.href=';
			echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($filepath . 'src/Users.php')) /* line 18 */;
			echo '">zavřít</button>
';
		}
		else {
			echo '        <button onclick= "location.href=';
			echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($filepath . $url)) /* line 20 */;
			echo '">zavřít</button>
';
		}
		echo '        </div>
    </div>

</div>
';
		return get_defined_vars();
	}

}
