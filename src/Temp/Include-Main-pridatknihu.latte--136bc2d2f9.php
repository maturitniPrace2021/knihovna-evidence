<?php
// source: ../../template/Include/Main/pridatknihu.latte

use Latte\Runtime as LR;

final class Template136bc2d2f9 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		if (isset($_SESSION['admin']) AND $_SESSION['admin'] == 1) {
			echo '<section class="Add_remove_book">
    <div class="whatisit">
        <div><h2>Přidat knihu</h2></div>
        <button onclick="goBack()">Zpátky</button>

    </div>
    <article>
        <form id="ADD" action="../Formaction/Pridatknihu.php" method="post">
            <div class="first">
                <div class="div">
                    <div class="label">
                        <label for="name">Název Knihy:</label>
                    </div>
                    <div class="input_content_canfill">
                        <input type="text"id="name" name="name" required>
                    </div>
                </div>
                <div class="div">
                    <div class="label">
                        <label for="author">Autor:</label>
                    </div>
                    <div class="input_content_canfill">
                        <input type="text"id="author" name="author" required>
                    </div>
                </div>
                <div class="div">
                    <div class="label">
                        <label for="isbn">ISBN:</label>
                    </div>
                    <div class="input_content_canfill">
                        <input type="text"id="isbn" name="isbn" required>
                    </div>
                </div>
                <div class="div">
                    <div class="label">
                        <label for="warehouse">Počet kusů:</label>
                    </div>
                    <div class="input_content_canfill">
                        <input type="number" min="0" id="warehouse" name="warehouse" value="0">
                    </div>
                </div>


            </div>
            <div class="secound">
                <div class="button">
                    <button type="submit" form="ADD">Uložit</button>
                </div>
            </div>
        </form>
    </article>
</section>
';
		}
		else {
			echo '<script>alert("Nemáte oprávnění")</script>
';
		}
		return get_defined_vars();
	}

}
