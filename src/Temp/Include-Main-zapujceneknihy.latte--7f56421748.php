<?php
// source: ../template/Include/Main/zapujceneknihy.latte

use Latte\Runtime as LR;

final class Template7f56421748 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<section class="zapujceneknihyall">
';
		$url = './Detail/Zapujceneknihydetail.php?id=';
		$url2 = './Detail/Vratit.php?id=';
		$url3 = 'Detail/Vratitknihuzanekohodetail.php?id=';
		$latereturn = date('Y-m-d');
		echo '


';
		$iterations = 0;
		foreach ($LendBooks as $Books) {
			$iterations = 0;
			foreach ($Books as $value => $key) {
				$ʟ_switch = ($value);
				if (false) {
				}
				elseif (in_array($ʟ_switch, [9], true)) {
					$date_lend = $key;
				}
				elseif (in_array($ʟ_switch, [10], true)) {
					$date_return = $key;
				}
				elseif (in_array($ʟ_switch, ['id'], true)) {
					$id = $key;
				}
				elseif (in_array($ʟ_switch, ['date_return'], true)) {
					$date_returnif = $key;
				}
				elseif (in_array($ʟ_switch, ['name'], true)) {
					$name = $key;
				}
				elseif (in_array($ʟ_switch, ['author'], true)) {
					$author = $key;
				}
				elseif (in_array($ʟ_switch, ['number'], true)) {
					$number = $key;
				}
				elseif (in_array($ʟ_switch, ['users_name'], true)) {
					$users_name = $key;
				}
				echo "\n";
				$iterations++;
			}
			if ($name == "žádná data") {
				echo '      <h2>žádná data</h2>
';
			}
			else {
				if (isset($_GET['vratit']) AND $_GET['vratit'] == "true") {
					echo '        <a href=';
					echo LR\Filters::escapeHtmlAttrUnquoted(LR\Filters::safeUrl($url2 . $id)) /* line 35 */;
					echo '>
';
				}
				elseif (isset($_GET['vratitall']) AND $_GET['vratitall'] == "true") {
					echo '        <a href=';
					echo LR\Filters::escapeHtmlAttrUnquoted(LR\Filters::safeUrl($url3 . $id)) /* line 37 */;
					echo '>
';
				}
				else {
					echo '        <a href=';
					echo LR\Filters::escapeHtmlAttrUnquoted(LR\Filters::safeUrl($url . $id)) /* line 39 */;
					echo '>
';
				}
				echo '      ';
				if ($date_returnif <= $latereturn) {
					echo '<article class="latereturn">';
				}
				else {
					echo ' <article> ';
				}
				echo '
      <h2>';
				echo LR\Filters::escapeHtmlText($name) /* line 42 */;
				echo '</h2>
      <div>
      <p>';
				echo LR\Filters::escapeHtmlText($author) /* line 44 */;
				echo '</p>
      <p>Počet zapujčených kusů:<br>';
				echo LR\Filters::escapeHtmlText($number) /* line 45 */;
				echo '</p>
';
				if (isset($_GET['vratitall']) AND $_GET['vratitall'] == "true") {
					echo '        <p>';
					echo LR\Filters::escapeHtmlText($users_name) /* line 47 */;
					echo '</p>
';
				}
				echo '
      <p>';
				echo LR\Filters::escapeHtmlText($date_lend) /* line 50 */;
				echo ' - ';
				echo LR\Filters::escapeHtmlText($date_return) /* line 50 */;
				echo '</p>
      </div>
      </article>
      </a>
';
			}
			$iterations++;
		}
		echo '

</section>
';
		return get_defined_vars();
	}


	public function prepare(): void
	{
		extract($this->params);
		if (!$this->getReferringTemplate() || $this->getReferenceType() === "extends") {
			foreach (array_intersect_key(['value' => '10', 'key' => '10', 'Books' => '9'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		
	}

}
