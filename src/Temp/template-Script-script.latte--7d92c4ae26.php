<?php
// source: ../../template/Script/script.latte

use Latte\Runtime as LR;

final class Template7d92c4ae26 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<script>
        function goBack(){
            window.history.back();
           }
</script>
<script>
   function Hamburger(){
            const navSlide = () => {
            const burger = document.querySelector(\'.burger\');
            const nav = document.querySelector(\'.nav\');
            const navLinks = document.querySelectorAll(\'.nav li, .nav form\');
            const n = document.querySelector(\'nav\');

            burger.addEventListener(\'click\', () => {
                nav.classList.toggle(\'nav-active\');

                navLinks.forEach((link, index) => {
                if(link.style.animation){
                    link.style.animation = \'\';
                }
                else{
                    link.style.animation = \'navLinkFade 0.5s ease forwards ${\'+index+\' / 7 + 0.5}s\';
                }
                });

                burger.classList.toggle(\'toggle\');
                n.classList.toggle(\'n\');
            });


        }

        navSlide();
        }

        Hamburger();

</script>
<script type="text/javascript" src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($filepath)) /* line 39 */;
		echo '/backend/function/autocomplete.js"></script><script>
var autocompletearray = ';
		echo LR\Filters::escapeJs($_SESSION['autocomplete']) /* line 40 */;
		echo ';
var searchinput = document.getElementById("searchinput");
autocomplete(searchinput, autocompletearray);
</script>
';
		return get_defined_vars();
	}

}
