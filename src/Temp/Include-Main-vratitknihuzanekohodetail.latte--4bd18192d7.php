<?php
// source: ../../template/Include/Main/vratitknihuzanekohodetail.latte

use Latte\Runtime as LR;

final class Template4bd18192d7 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		if (isset($_SESSION['admin']) AND $_SESSION['admin'] == 1) {
			echo '<section class="return">
    <div class="whatisit">
        <h2>Vrátit knihu za někoho</h2>
        <button onclick="goBack()">Zpátky</button>

    </div>
    <article>
        <form id="RETURN" action="../Formaction/Vratitindex.php" method="post">
            <div class="first">
                <div class="div">
                    <div class="label">
                        <label>Název Knihy:</label>
                    </div>
                    <div class="input_content_cantfill">
                        <p>';
			echo LR\Filters::escapeHtmlText($name_book) /* line 16 */;
			echo '<p>
                    </div>
                </div>
                <div class="div">
                    <div class="label">
                        <label>Autor:</label>
                    </div>
                    <div class="input_content_cantfill">
                        <p>';
			echo LR\Filters::escapeHtmlText($author) /* line 24 */;
			echo '<p>
                    </div>
                </div>
                <div class="div">
                    <div class="label">
                        <label>Čtenář:</label>
                    </div>
                    <div class="input_content_cantfill">
                        <p>';
			echo LR\Filters::escapeHtmlText($reader) /* line 32 */;
			echo '<p>
                    </div>
                </div>
                <div class="div">
                    <div class="label">
                        <label for="user_warehouse">Počet kusů:</label>
                    </div>
                    <div class="input_content_cantfill">
                         <p>';
			echo LR\Filters::escapeHtmlText($userwarehouse) /* line 40 */;
			echo '<p>
                    </div>
                </div>
            </div>

            <div class="secound">
                <div class="textarea">
                    <div class="label">
                        <label>Poznámka k vrácení</label>
                    </div>
                        <textarea id="note" name="note" placeholder="Poznámka..."></textarea>

                </div>
            </div>

            <div class="third">
                <div class="button">
                  <input type="hidden" value=';
			echo LR\Filters::escapeHtmlAttrUnquoted($id_lendedbooks) /* line 57 */;
			echo ' name= "id_lendedbooks">

                    <button type="submit" form="RETURN">Vrátit</button>
                </div>
            </div>
        </form>

    </article>
</section>
';
		}
		else {
			echo '<script>alert("Nemáte oprávnění")</script>
';
		}
		return get_defined_vars();
	}

}
