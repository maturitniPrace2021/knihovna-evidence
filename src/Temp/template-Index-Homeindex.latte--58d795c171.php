<?php
// source: ../template/Index/Homeindex.latte

use Latte\Runtime as LR;

final class Template58d795c171 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../frontend/dist/style_less.css">

    <title>Home</title>
</head>
<body>
    <header>
';
		/* line 15 */
		$this->createTemplate('../Include/Header/header.latte', $this->params, 'include')->renderToContentType('html');
		echo '    </header>

';
		if (isset($_GET['error'])) {
			/* line 19 */
			$this->createTemplate('../Include/Error/error.latte', $this->params, 'include')->renderToContentType('html');
		}
		echo '    <nav>
';
		/* line 22 */
		$this->createTemplate('../Include/Nav/nav.latte', $this->params, 'include')->renderToContentType('html');
		echo '    </nav>

    <main>
';
		if (isset($_SESSION['admin']) AND $_SESSION['admin'] == 1) {
			/* line 27 */
			$this->createTemplate('../Include/Main/mainadministrator.latte', $this->params, 'include')->renderToContentType('html');
		}
		else {
			/* line 29 */
			$this->createTemplate('../Include/Main/main.latte', $this->params, 'include')->renderToContentType('html');
		}
		echo '    </main>

    <footer>
';
		/* line 34 */
		$this->createTemplate('../Include/Footer/footer.latte', $this->params, 'include')->renderToContentType('html');
		echo '    </footer>
';
		/* line 36 */
		$this->createTemplate('../Script/script.latte', $this->params, 'include')->renderToContentType('html');
		echo '</body>
</html>
';
		return get_defined_vars();
	}

}
