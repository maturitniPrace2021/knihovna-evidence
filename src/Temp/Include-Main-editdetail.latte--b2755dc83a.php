<?php
// source: ../../template/Include/Main/editdetail.latte

use Latte\Runtime as LR;

final class Templateb2755dc83a extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		if (isset($_SESSION['admin']) AND $_SESSION['admin'] == 1) {
			echo '<section class="editdetail">
';
			$returnurl = './Detail/Vratit.php?id=';
			$lendurl = './Detail/Pujcit.php?id=';
			echo "\n";
			$iterations = 0;
			foreach ($detailBook as $value => $key) {
				$ʟ_switch = ($value);
				if (false) {
				}
				elseif (in_array($ʟ_switch, ['name'], true)) {
					$name_book = $key;
				}
				elseif (in_array($ʟ_switch, ['warehouse'], true)) {
					$warehouse = $key;
				}
				elseif (in_array($ʟ_switch, ['author'], true)) {
					$author = $key;
				}
				elseif (in_array($ʟ_switch, ['isbn'], true)) {
					$isbn = $key;
				}
				$iterations++;
			}
			echo '





    <div class="whatisit">
        <h2>Editovat knihu</h2>
        <button onclick="goBack()">Zpátky</button>
    </div>
    <article>
        <form class="form" id="EDIT" action="../Formaction/Editovatknihuindex.php" method="post">
          <input type="hidden" name="id" value="';
			echo LR\Filters::escapeHtmlAttr($id_books) /* line 30 */;
			echo '">
            <div class="first">
                <div class="div">
                    <div class="label">
                        <label>Název Knihy:</label>
                    </div>
                    <div class="input_content_canfill">
                        <input type="text" name="name_book" id="name_book" value="';
			echo LR\Filters::escapeHtmlAttr($name_book) /* line 37 */;
			echo '">
                    </div>
                </div>
                <div class="div">
                    <div class="label">
                        <label>Název autora:</label>
                    </div>
                    <div class="input_content_canfill">
                        <input type="text" name="author" id="author" value="';
			echo LR\Filters::escapeHtmlAttr($author) /* line 45 */;
			echo '">
                    </div>
                </div>
                <div class="div">
                    <div class="label">
                        <label>ISBN:</label>
                    </div>
                    <div class="input_content_canfill">
                        <input type="text" name="isbn" id="isbn" value="';
			echo LR\Filters::escapeHtmlAttr($isbn) /* line 53 */;
			echo '">
                    </div>
                </div>
                <div class="div">
                    <div class="label">
                        <label>Počet kusů na skladě:</label>
                    </div>
                    <div class="input_content_canfill">
                        <input type="number" min="0" max="number" id="warehouse" name="warehouse" value="';
			echo LR\Filters::escapeHtmlAttr($warehouse) /* line 61 */;
			echo '">
                    </div>
                </div>
            </div>

            <div class="secound">
                <div class="textarea">
                    <div class="label">
                        <label>Poznámky k vypůjčení</label>
                    </div>
                    <div class="notes">
';
			$iterations = 0;
			foreach ($lendNotes as $lNotes) {
				$iterations = 0;
				foreach ($lNotes as $value => $key) {
					$ʟ_switch = ($value);
					if (false) {
					}
					elseif (in_array($ʟ_switch, ['id'], true)) {
						$id_note = $key;
					}
					elseif (in_array($ʟ_switch, ['note'], true)) {
						$note = $key;
					}
					elseif (in_array($ʟ_switch, ['changes'], true)) {
						$changes = $key;
					}
					elseif (in_array($ʟ_switch, ['name'], true)) {
						$name = $key;
					}
					elseif (in_array($ʟ_switch, ['username'], true)) {
						$username = $key;
					}
					$iterations++;
				}
				if ($note == "žádné poznámky") {
					echo '                              <p> žádné poznámky </p>
';
				}
				else {
					echo '
                    <div class="note-container">
                      <div class="delete">
                        <a href=';
					echo LR\Filters::escapeHtmlAttrUnquoted(LR\Filters::safeUrl($filepath . 'src/Formaction/DeleteNote.php?id_note=' . $id_note . '&id_books=' . $_GET['id'])) /* line 93 */;
					echo '><i class="fa fa-times"></i></a>
                      </div>
                      <div class="note">
                        <p>Poznámka:<br>
                        ';
					echo LR\Filters::escapeHtmlText($note) /* line 97 */;
					echo '<br>

                        Přidáno dne: ';
					echo LR\Filters::escapeHtmlText($changes) /* line 99 */;
					echo '<br>

                        Přidáno uživatelem: ';
					echo LR\Filters::escapeHtmlText($username) /* line 101 */;
					echo '<br>
                        _______________________</p>
                      </div>
                    </div>
';
				}
				$iterations++;
			}
			echo '                  </div>
                </div>
              </div>

              <div class="third">

                <div class="textarea">
                    <div class="label">
                        <label>Poznámky k vrácení</label>
                    </div>
                    <div class="notes">
';
			$iterations = 0;
			foreach ($returnNotes as $rNotes) {
				$iterations = 0;
				foreach ($rNotes as $value => $key) {
					$ʟ_switch = ($value);
					if (false) {
					}
					elseif (in_array($ʟ_switch, ['id'], true)) {
						$id_note = $key;
					}
					elseif (in_array($ʟ_switch, ['note'], true)) {
						$note = $key;
					}
					elseif (in_array($ʟ_switch, ['changes'], true)) {
						$changes = $key;
					}
					elseif (in_array($ʟ_switch, ['name'], true)) {
						$name = $key;
					}
					elseif (in_array($ʟ_switch, ['username'], true)) {
						$username = $key;
					}
					$iterations++;
				}
				if ($note == "žádné poznámky") {
					echo '                        <p> žádné poznámky </p>
';
				}
				else {
					echo '
                      <div class="note-container">
                        <div class="delete">
                        <a href=';
					echo LR\Filters::escapeHtmlAttrUnquoted(LR\Filters::safeUrl($filepath . 'src/Formaction/DeleteNote.php?id_note=' . $id_note . '&id_books=' . $_GET['id'])) /* line 139 */;
					echo '><i class="fa fa-times"></i></a>
                      </div>
                      <div class="note">
                        <p>Poznámka:<br>
                        ';
					echo LR\Filters::escapeHtmlText($note) /* line 143 */;
					echo '<br>

                        Přidáno dne: ';
					echo LR\Filters::escapeHtmlText($changes) /* line 145 */;
					echo '<br>

                        Přidáno uživatelem: ';
					echo LR\Filters::escapeHtmlText($username) /* line 147 */;
					echo '<br>
                        _______________________</p></div></div>
';
				}
				$iterations++;
			}
			echo '                      </div>
                    </div>
                  </div>

            <div class="four">
                <div class="button">
                    <button type="submit" form="EDIT">Uložit</button>
                </div>
            </div>
        </form>
    </article>

</section>
';
		}
		else {
			echo '<script>alert("Nemáte oprávnění")</script>
';
		}
		return get_defined_vars();
	}


	public function prepare(): void
	{
		extract($this->params);
		if (!$this->getReferringTemplate() || $this->getReferenceType() === "extends") {
			foreach (array_intersect_key(['value' => '6, 73, 119', 'key' => '6, 73, 119', 'lNotes' => '72', 'rNotes' => '118'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		
	}

}
