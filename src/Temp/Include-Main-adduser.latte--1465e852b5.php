<?php
// source: ../template/Include/Main/adduser.latte

use Latte\Runtime as LR;

final class Template1465e852b5 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		if (isset($_SESSION['admin']) AND $_SESSION['admin'] == 1) {
			echo '<section class="navadd">
    <div class="form">
        <a href="Detail/Adduser.php"><span class="fa fa-plus"></span></a>
    </div>
<section>
';
		}
		else {
			echo '<script>alert("Nemáte oprávnění")</script>
';
		}
		return get_defined_vars();
	}

}
