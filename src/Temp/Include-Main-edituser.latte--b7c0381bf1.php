<?php
// source: ../../template/Include/Main/edituser.latte

use Latte\Runtime as LR;

final class Templateb7c0381bf1 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '
<section class="edituser">

    <div class="whatisit">
        <h2>User</h2>
        <button onclick="goBack()">Zpátky</button>
    </div>
    <article>
        <form class="form" id="Useredit" action="../Formaction/Edituserindex.php" method="post">
            <div class="first">
                <div class="div">
                    <div class="label">
                        <label>Jméno uživatele</label>
                    </div>
                    <div class="input_content_canfill">
                        <input type="text" name="name" id="name_user" value="';
		echo LR\Filters::escapeHtmlAttr($name) /* line 16 */;
		echo '">
                    </div>
                </div>

                <div class="div">
                    <div class="label">
                        <label>Uživatelské jméno</label>
                    </div>
                    <div class="input_content_canfill">
                        <input type="text" name="username" id="name_user" value="';
		echo LR\Filters::escapeHtmlAttr($username) /* line 25 */;
		echo '">
                    </div>
                </div>

                <div class="div">
                    <div class="label">
                        <label for="password">Nastavit nové heslo:</label>

                        </div>
                        <div class="input_content_canfill">
                            <input type="password" name="password" id="name_user">
                        </div>
                </div>

                <div class="div">
                    <div class="label">
                        <label for="password1">Heslo znovu:</label>
                    </div>
                    <div class="input_content_canfill">
                        <input type="password"id="password" name="password1">
                    </div>

                </div>

';
		if (isset($_SESSION['admin']) AND $_SESSION['admin'] == 1) {
			echo '                    <div class="div">
                        <div class="label">
                            <label>Oprávnění</label>

                        </div>
                        <div class="input_content_canfill">

                            <select name="admin">
                            <option value = \'1\' ';
			if ($admin == 1) {
				echo ' selected ';
			}
			echo '>admin</option>
                            <option value = \'0\' ';
			if ($admin == 0) {
				echo ' selected ';
			}
			echo '>uživatel</option>
                            </select>

                        </div>
                    </div>
';
		}
		echo '                        <input type="hidden" value="';
		echo LR\Filters::escapeHtmlAttr($_GET['id']) /* line 65 */;
		echo '" name=\'id\'>


            </div>

            <div class="secound">
                <div class="button">
                    <button type="submit" form="Useredit">Uložit</button>
                </div>
            </div>
        </form>
    </article>

</section>
';
		return get_defined_vars();
	}

}
