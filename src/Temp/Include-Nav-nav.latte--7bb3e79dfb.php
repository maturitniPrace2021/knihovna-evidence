<?php
// source: template\Include\Nav\nav.latte

use Latte\Runtime as LR;

final class Template7bb3e79dfb extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<div class="burger">
    <div class="line1"></div>
    <div class="line2"></div>
    <div class="line3"></div>
</div>

<div class="nav">

    <ul class="nav-items">
        <li><a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($filepath)) /* line 10 */;
		echo 'Index.php">Home</a></li>
        <li><a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($filepath)) /* line 11 */;
		echo 'src/Rad.php">Řád</a></li>
        <li><a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($filepath)) /* line 12 */;
		echo 'src/Knihyindex.php">Knihy</a></li>
    </ul>

    <form id="autocompleteform" autocomplete="off" action="" method="get">
';
		if (isset($_GET['search'])) {
			$search = $_GET['search'];
		}
		else {
			$search = '';
		}
		echo '        <div id="autocompletediv" class="input" style="position:relative">
        <input id="searchinput" type="search" class="search-data" name="search" value=';
		echo LR\Filters::escapeHtmlAttrUnquoted($search) /* line 22 */;
		echo ' placeholder="Hledat...">
        </div>
';
		$iterations = 0;
		foreach ($_GET as $name => $value) {
			if ($name != 'search') {
				echo '          <input type=hidden name = ';
				echo LR\Filters::escapeHtmlAttrUnquoted($name) /* line 26 */;
				echo ' value = ';
				echo LR\Filters::escapeHtmlAttrUnquoted($value) /* line 26 */;
				echo '>
';
			}
			$iterations++;
		}
		echo '        <button type="submit"><span class="fa fa-search"></span></button>
    </form>
</div>
';
		return get_defined_vars();
	}


	public function prepare(): void
	{
		extract($this->params);
		if (!$this->getReferringTemplate() || $this->getReferenceType() === "extends") {
			foreach (array_intersect_key(['name' => '24', 'value' => '24'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		
	}

}
