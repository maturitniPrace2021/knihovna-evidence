<?php
// source: ../template/Index/Zapujceneknihy.latte

use Latte\Runtime as LR;

final class Templatedabd8f7a09 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Knihovna knih">
    <meta name="keywords" content="HTML, CSS, JavaScript, Latte, Less, PHP, MySql">
    <meta name="author" content="Filip Černý, Tobiáš Procházka">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../frontend/dist/style_less.css">

    <title>
';
		if ($vratit == "true") {
			echo '        Vrátit knihu
';
		}
		elseif ($vratitall=="true") {
			echo '        Vrátit knihu za někoho
';
		}
		else {
			echo '        Zapůjčené knihy
';
		}
		echo '    </title>
</head>
<body>
    <header>
';
		/* line 26 */
		$this->createTemplate('../Include/Header/header.latte', $this->params, 'include')->renderToContentType('html');
		echo '    </header>

';
		if (isset($_GET['error'])) {
			/* line 30 */
			$this->createTemplate('../Include/Error/error.latte', $this->params, 'include')->renderToContentType('html');
		}
		echo '
    <nav>
';
		/* line 34 */
		$this->createTemplate('../Include/Nav/nav.latte', $this->params, 'include')->renderToContentType('html');
		echo '
    </nav>
';
		if (isset($_GET['vratitall']) AND $_GET['vratitall'] == 'true') {
			echo '        <nav class="secoundnav">
';
			/* line 39 */
			$this->createTemplate('../Include/Nav/secondnav.latte', $this->params, 'include')->renderToContentType('html');
			echo '        </nav>
';
		}
		echo '    <main>
';
		if (isset($_SESSION['user_id'])) {
			if (isset($_GET['vratitall'])) {
				echo '        <h2 style="padding-bottom: 50px; color: white;">Vrátit knihu za někoho</h2>
';
			}
			elseif (isset($_GET['vratit'])) {
				echo '        <h2 style="padding-bottom: 50px; color: white;">Vrátit knihu</h2>
';
			}
			else {
				echo '        <h2 style="padding-bottom: 50px; color: white;">Zapůjčené knihy</h2>
';
			}
			echo '

';
			/* line 53 */
			$this->createTemplate('../Include/Main/zapujceneknihy.latte', $this->params, 'include')->renderToContentType('html');
		}
		else {
			/* line 55 */
			$this->createTemplate('../Include/Main/prihlastese.latte', $this->params, 'include')->renderToContentType('html');
		}
		echo '    </main>

    <footer>
';
		/* line 60 */
		$this->createTemplate('../Include/Footer/footer.latte', $this->params, 'include')->renderToContentType('html');
		echo '    </footer>

';
		/* line 63 */
		$this->createTemplate('../Script/script.latte', $this->params, 'include')->renderToContentType('html');
		echo '</body>
</html>
';
		return get_defined_vars();
	}

}
