<?php
// source: ../../template/Include/Main/zapujceneknihydetail.latte

use Latte\Runtime as LR;

final class Template44f160ddfa extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<section class="zapujceneknihy">
';
		$returnurl = $filepath . 'src/Detail/Vratit.php?id=';
		echo "\n";
		$iterations = 0;
		foreach ($detailBook as $value => $key) {
			$ʟ_switch = ($value);
			if (false) {
			}
			elseif (in_array($ʟ_switch, ['name'], true)) {
				$name_book = $key;
			}
			elseif (in_array($ʟ_switch, ['author'], true)) {
				$author = $key;
				echo "\n";
			}
			$iterations++;
		}
		echo '  

    <div class="whatisit">
          <h2>Zapůjčená kniha</h2>
          <button onclick="goBack()">Zpátky</button>
      </div>
      <article>
          <div class="form">
                <div class="first">
                    <div class="div">
                        <div class="label">
                            <label>Název Knihy:</label>
                        </div>
                        <div class="input_content_cantfill">
                            <p>';
		echo LR\Filters::escapeHtmlText($name_book) /* line 27 */;
		echo '</p>
                        </div>
                    </div>

                    <div class="div">
                        <div class="label">
                            <label>Zapůjčeno od:</label>
                        </div>
                        <div class="input_content_cantfill">
                            <p>';
		echo LR\Filters::escapeHtmlText($date_lend) /* line 36 */;
		echo '</p>
                        </div>
                    </div>

                    <div class="div">
                        <div class="label">
                            <label>Zapůjčeno do:</label>
                        </div>
                        <div class="input_content_cantfill">
                            <p>';
		echo LR\Filters::escapeHtmlText($date_return) /* line 45 */;
		echo '</p>
                        </div>
                    </div>

                    <div class="div">
                        <div class="label">
                            <label>Počet kusů:</label>
                        </div>
                        <div class="input_content_cantfill">
                            <p>';
		echo LR\Filters::escapeHtmlText($number) /* line 54 */;
		echo '</p>
                        </div>
                    </div>
                </div>

                <div class="secound">
                    <div class="textarea">
                        <div class="label">
                            <label>Poznámky k vypůjčení</label>
                        </div>
                        <textarea disabled>
';
		$iterations = 0;
		foreach ($lendNotes as $lNotes) {
			$iterations = 0;
			foreach ($lNotes as $value => $key) {
				$ʟ_switch = ($value);
				if (false) {
				}
				elseif (in_array($ʟ_switch, ['username'], true)) {
					echo 'Přidáno uživatelem: ';
					echo LR\Filters::escapeHtmlText($key) /* line 69 */;
					echo '
_______________________

';
				}
				elseif (in_array($ʟ_switch, ['note'], true)) {
					echo 'Poznámka:
';
					echo LR\Filters::escapeHtmlText($key) /* line 74 */;
					echo '

';
				}
				elseif (in_array($ʟ_switch, ['changes'], true)) {
					echo 'Přidáno dne: ';
					echo LR\Filters::escapeHtmlText($key) /* line 77 */;
					echo "\n";
				}
				$iterations++;
			}
			$iterations++;
		}
		echo '</textarea>
                    </div>
                </div>

                <div class="third">
                    <div class="button">
                        <button onclick= "location.href=';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($returnurl . $_GET['id'])) /* line 87 */;
		echo '">Vrátit</button>
                    </div>
                </div>
          </div>
</article>
</section>
';
		return get_defined_vars();
	}


	public function prepare(): void
	{
		extract($this->params);
		if (!$this->getReferringTemplate() || $this->getReferenceType() === "extends") {
			foreach (array_intersect_key(['value' => '4, 66', 'key' => '4, 66', 'lNotes' => '65'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		
	}

}
