<?php
// source: ../../template/Index/Adduserdetail.latte

use Latte\Runtime as LR;

final class Templateaab793786d extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Knihovna knih">
    <meta name="keywords" content="HTML, CSS, JavaScript, Latte, Less, PHP, MySql">
    <meta name="author" content="Filip Černý, Tobiáš Procházka">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../frontend/dist/style_less.css">

    <title>Přidat uživatele</title>
</head>
<body>
    <header>
';
		/* line 18 */
		$this->createTemplate('../Include/Header/header.latte', $this->params, 'include')->renderToContentType('html');
		echo '    </header>
';
		if (isset($_GET['error'])) {
			/* line 21 */
			$this->createTemplate('../Include/Error/error.latte', $this->params, 'include')->renderToContentType('html');
		}
		echo '    <nav>
';
		/* line 24 */
		$this->createTemplate('../Include/Nav/nav.latte', $this->params, 'include')->renderToContentType('html');
		echo '    </nav>

    <main>
';
		/* line 28 */
		$this->createTemplate('../Include/Main/adduserdetail.latte', $this->params, 'include')->renderToContentType('html');
		echo '    </main>

    <footer>
';
		/* line 32 */
		$this->createTemplate('../Include/Footer/footer.latte', $this->params, 'include')->renderToContentType('html');
		echo '    </footer>

';
		/* line 35 */
		$this->createTemplate('../Script/script.latte', $this->params, 'include')->renderToContentType('html');
		echo '</body>
</html>
';
		return get_defined_vars();
	}

}
