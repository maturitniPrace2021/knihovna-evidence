<?php
// source: ..\..\template\Include\Main\mainlogin.latte

use Latte\Runtime as LR;

final class Template845d3aaf04 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<section class="login">
    <div class="logmenu">

        <h2>Přihlášení</h2>
        <form action="../Formaction/Loginindex.php" method="post" id="loginform">
            <div>
                <input type="text" name="username" placeholder="Uživatelské jméno" autofocus required>
            </div>
            <div>
                <input type="password" name="password" placeholder="Heslo" required>
            </div>
        </form>
        <button type="submit" form="loginform">Příhlásit se</button>
    </div>
</section>
';
		return get_defined_vars();
	}

}
