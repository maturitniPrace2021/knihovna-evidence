<?php
// source: ../template/Include/Nav/secondnav.latte

use Latte\Runtime as LR;

final class Templatee0191acb4d extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '    <div class="nav">
      <form action="" method="get">
        <select name="search_user_id" onchange="this.form.submit();">
            <option value=\'all\'>Vyber čtenáře</option>
';
		$iterations = 0;
		foreach ($users as $user) {
			$iterations = 0;
			foreach ($user as $value => $key) {
				echo '                ';
				if ($value == 'id') {
					$id = $key;
				}
				echo '
                ';
				if ($value == 'name') {
					$name = $key;
				}
				echo "\n";
				$iterations++;
			}
			echo '              <option value=';
			echo LR\Filters::escapeHtmlAttrUnquoted($id) /* line 10 */;
			if (isset($_GET['search_user_id']) AND $_GET['search_user_id']==$id) {
				echo ' selected';
			}
			echo '>';
			echo LR\Filters::escapeHtmlText($name) /* line 10 */;
			echo '</option>
';
			$iterations++;
		}
		$iterations = 0;
		foreach ($_GET as $name => $value) {
			if ($name != 'search_user_id') {
				echo '              <input type=hidden name = ';
				echo LR\Filters::escapeHtmlAttrUnquoted(htmlspecialchars($name)) /* line 14 */;
				echo ' value = ';
				echo LR\Filters::escapeHtmlAttrUnquoted(htmlspecialchars($value)) /* line 14 */;
				echo '>
';
			}
			$iterations++;
		}
		echo '        </select>
      </form>
    </div>
';
		return get_defined_vars();
	}


	public function prepare(): void
	{
		extract($this->params);
		if (!$this->getReferringTemplate() || $this->getReferenceType() === "extends") {
			foreach (array_intersect_key(['value' => '6, 12', 'key' => '6', 'user' => '5', 'name' => '12'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		
	}

}
