<?php
// source: ..\template\Include\Main\mainusers.latte

use Latte\Runtime as LR;

final class Templatefe0c1da688 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		if (isset($_SESSION['admin']) AND $_SESSION['admin'] == 1) {
			echo '<section class="useredit">
';
			$edit = 'Detail/Edituser.php?id=';
			$delete = 'Formaction/Odebratuzivatele.php?id=';
			echo "\n";
			if (empty($users)) {
				echo '<h2>žádná data</h2>
';
			}
			else {
				echo '

';
				$iterations = 0;
				foreach ($users as $user) {
					$iterations = 0;
					foreach ($user as $value => $key) {
						$ʟ_switch = ($value);
						if (false) {
						}
						elseif (in_array($ʟ_switch, ['id'], true)) {
							$id_users = $key;
						}
						elseif (in_array($ʟ_switch, ['username'], true)) {
							$username = $key;
						}
						elseif (in_array($ʟ_switch, ['name'], true)) {
							echo '          <div class="a">
            <article>
              <div class="first">
                <div class="name">
                  <h2>';
							echo LR\Filters::escapeHtmlText($key) /* line 23 */;
							echo '</h2>
                </div>

                <div class="position">
                <p>';
							echo LR\Filters::escapeHtmlText($username) /* line 27 */;
							echo '</p>
';
						}
						elseif (in_array($ʟ_switch, ['admin'], true)) {
							if ($key == 1) {
								echo '                    <p>admin</p>
';
							}
							else {
								echo '                    <p>uživatel</p>
';
							}
							echo '
                </div>
              </div>
              <div class="secound">
                  <div class="button">
                    <button onclick= "location.href=';
							echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($delete . $id_users)) /* line 39 */;
							echo '">Odebrat</button>
                  </div>

                  <div class="button">
                    <button onclick= "location.href=';
							echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($edit . $id_users)) /* line 43 */;
							echo '">Editovat</button>
                  </div>
            </article>
          </div>

';
						}
						$iterations++;
					}
					echo "\n";
					$iterations++;
				}
				echo '  </a>
';
			}
			echo '</section>
';
		}
		else {
			echo '<script>alert("Nemáte oprávnění")</script>
';
		}
		return get_defined_vars();
	}


	public function prepare(): void
	{
		extract($this->params);
		if (!$this->getReferringTemplate() || $this->getReferenceType() === "extends") {
			foreach (array_intersect_key(['value' => '12', 'key' => '12', 'user' => '11'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		
	}

}
