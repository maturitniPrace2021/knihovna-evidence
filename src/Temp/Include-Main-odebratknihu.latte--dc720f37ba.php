<?php
// source: ../../template/Include/Main/odebratknihu.latte

use Latte\Runtime as LR;

final class Templatedc720f37ba extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		if (isset($_SESSION['admin']) AND $_SESSION['admin'] == 1) {
			echo '<section class="Add_remove_book">
    <div class="whatisit">
        <div><h2>Odebrat knihu</h2></div>
        <button onclick="goBack()">Zpátky</button>

    </div>
    <article>
        <form id="REMOVE" action="../Formaction/Odebratknihuindex.php" method="post">
            <div class="first">
                <div class="div">
                    <div class="label">
                        <label>Název Knihy:</label>
                    </div>
                    <div class="input_content_cantfill">
                        <p>';
			echo LR\Filters::escapeHtmlText($name_book) /* line 16 */;
			echo '</p>
                    </div>
                </div>
                <div class="div">
                    <div class="label">
                        <label>Autor:</label>
                    </div>
                    <div class="input_content_cantfill">
                        <p>';
			echo LR\Filters::escapeHtmlText($author) /* line 24 */;
			echo '</p>
                    </div>
                </div>
                <div class="div">
                    <div class="label">
                        <label>ISBN:</label>
                    </div>
                    <div class="input_content_cantfill">
                        <textarea disabled wrap="off">';
			echo LR\Filters::escapeHtmlText($isbn) /* line 32 */;
			echo '</textarea>
                    </div>
                </div>
            </div>

            <div class="secound">
              <input type="hidden" name="id_books" value="';
			echo LR\Filters::escapeHtmlAttr($id_books) /* line 38 */;
			echo '">

                <button type="submit" form="REMOVE">Smazat</button>
            </div>
        </form>
    </article>
</section>
';
		}
		else {
			echo '<script>alert("Nemáte oprávnění")</script>
';
		}
		return get_defined_vars();
	}

}
