<?php
// source: ../../template/Include/Main/editdetailcopy.latte

use Latte\Runtime as LR;

final class Templatebe615011a4 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		if (isset($_SESSION['admin']) AND $_SESSION['admin'] == 1) {
			echo '<section class="editdetail">
';
			$returnurl = './Detail/Vratit.php?id=';
			$lendurl = './Detail/Pujcit.php?id=';
			echo "\n";
			$iterations = 0;
			foreach ($detailBook as $value => $key) {
				$ʟ_switch = ($value);
				if (false) {
				}
				elseif (in_array($ʟ_switch, ['name'], true)) {
					$name_book = $key;
				}
				elseif (in_array($ʟ_switch, ['warehouse'], true)) {
					$warehouse = $key;
				}
				elseif (in_array($ʟ_switch, ['author'], true)) {
					$author = $key;
				}
				elseif (in_array($ʟ_switch, ['isbn'], true)) {
					$isbn = $key;
				}
				$iterations++;
			}
			echo '
    




    <div class="whatisit">
        <h2>Editovat knihu</h2>
        <button onclick="goBack()">Zpátky</button>
    </div>
    <article>
        <form class="form" id="EDIT" action="../Formaction/Editovatknihuindex.php" method="post">
          <input type="hidden" name="id" value="';
			echo LR\Filters::escapeHtmlAttr($id_books) /* line 30 */;
			echo '">
            <div class="first">
                <div class="div">
                    <div class="label">
                        <label>Název Knihy:</label>
                    </div>
                    <div class="input_content_canfill">
                        <input type="text" name="name_book" id="name_book" value="';
			echo LR\Filters::escapeHtmlAttr($name_book) /* line 37 */;
			echo '">
                    </div>
                </div>
                <div class="div">
                    <div class="label">
                        <label>Název autora:</label>
                    </div>
                    <div class="input_content_canfill">
                        <input type="text" name="author" id="author" value="';
			echo LR\Filters::escapeHtmlAttr($author) /* line 45 */;
			echo '">
                    </div>
                </div>
                <div class="div">
                    <div class="label">
                        <label>ISBN:</label>
                    </div>
                    <div class="input_content_canfill">
                        <input type="text" name="isbn" id="isbn" value="';
			echo LR\Filters::escapeHtmlAttr($isbn) /* line 53 */;
			echo '">
                    </div>
                </div>
                <div class="div">
                    <div class="label">
                        <label>Počet kusů na skladě:</label>
                    </div>
                    <div class="input_content_canfill">
                        <input type="number" min="0" max="number" id="warehouse" name="warehouse" value="';
			echo LR\Filters::escapeHtmlAttr($warehouse) /* line 61 */;
			echo '">
                    </div>
                </div>
            </div>

            <div class="secound">
                <div class="textarea">
                    <div class="label">
                        <label>Poznámky k vypůjčení</label>
                    </div>
                    <div class="notes">
';
			$iterations = 0;
			foreach ($lendNotes as $lNotes) {
				$iterations = 0;
				foreach ($lNotes as $value => $key) {
					$ʟ_switch = ($value);
					if (false) {
					}
					elseif (in_array($ʟ_switch, ['note'], true)) {
						echo '<div class="note-container">
<div class="delete">
<a href="#"><i class="fa fa-times"></i></a>
</div>
<div class="note">
<p>Poznámka:<br>
';
						echo LR\Filters::escapeHtmlText($key) /* line 82 */;
						echo '<br>
';
					}
					elseif (in_array($ʟ_switch, ['changes'], true)) {
						echo 'Přidáno dne: ';
						echo LR\Filters::escapeHtmlText($key) /* line 84 */;
						echo '<br>
';
					}
					elseif (in_array($ʟ_switch, ['username'], true)) {
						echo 'Přidáno uživatelem: ';
						echo LR\Filters::escapeHtmlText($key) /* line 86 */;
						echo '<br>
_______________________</p></div></div>
';
					}
					$iterations++;
				}
				$iterations++;
			}
			echo '</div>
                </div>
            </div>

            <div class="third">
                
                <div class="textarea">
                    <div class="label">
                        <label>Poznámky k vrácení</label>
                    </div>
                    <div class="note">
';
			$iterations = 0;
			foreach ($returnNotes as $rNotes) {
				$iterations = 0;
				foreach ($rNotes as $value => $key) {
					$ʟ_switch = ($value);
					if (false) {
					}
					elseif (in_array($ʟ_switch, ['note'], true)) {
						echo '<p>Poznámka:<br>
';
						echo LR\Filters::escapeHtmlText($key) /* line 107 */;
						echo '<br>
';
					}
					elseif (in_array($ʟ_switch, ['changes'], true)) {
						echo 'Přidáno dne: ';
						echo LR\Filters::escapeHtmlText($key) /* line 109 */;
						echo '<br>
';
					}
					elseif (in_array($ʟ_switch, ['username'], true)) {
						echo 'Přidáno uživatelem: ';
						echo LR\Filters::escapeHtmlText($key) /* line 111 */;
						echo '<br>
_______________________</p>
';
					}
					$iterations++;
				}
				$iterations++;
			}
			echo '</div>                    
                 </div>
            </div>

            <div class="four">
                <div class="button">
                    <button type="submit" form="EDIT">Uložit</button>
                </div>
            </div>
        </form>
    </article>

</section>
';
		}
		else {
			echo '<script>alert("Nemáte oprávnění")</script>
';
		}
		return get_defined_vars();
	}


	public function prepare(): void
	{
		extract($this->params);
		if (!$this->getReferringTemplate() || $this->getReferenceType() === "extends") {
			foreach (array_intersect_key(['value' => '6, 73, 103', 'key' => '6, 73, 103', 'lNotes' => '72', 'rNotes' => '102'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		
	}

}
