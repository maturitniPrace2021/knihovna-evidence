<?php
if(session_id() == ''){session_start();} if (isset($_SESSION['admin']) AND $_SESSION['admin'] == 1) {
require_once("../../include.php");

$obj_books = new Books();
$obj_books->setName(htmlspecialchars($_POST['name']));
$obj_books->setAuthor(htmlspecialchars($_POST['author']));
$obj_books->setIsbn(htmlspecialchars($_POST['isbn']));
$obj_books->setWarehouse(htmlspecialchars($_POST['warehouse']));
$obj_books->insertBook();

header("Location:../Detail/Knihadetail.php?id=".$obj_books->getId()."");
}
  else{
    echo '<script>alert("Nemáte oprávnění")</script>';
  }

 ?>
