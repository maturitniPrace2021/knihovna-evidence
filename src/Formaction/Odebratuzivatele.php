<?php
if(session_id() == ''){session_start();} if (isset($_SESSION['admin']) AND $_SESSION['admin'] == 1) {
require_once("../../include.php");

if (isset($_GET['deleteuser']) AND $_GET['deleteuser'] == true) { //vyskakovaci okno pro smazani
  $obj_users = new Users();
  $obj_users->deleteUser(htmlspecialchars($_GET['id']));

  //vratit knihy pod uzivatelem v lended_books
  $obj_lendedbooks = new Lended_books();
  $alllendebooks = $obj_lendedbooks->getAllLended_books();
  foreach ($alllendebooks as $key => $value) {
    if ($value['id_users'] == $_GET['id']) {
      $sql = "UPDATE lended_books SET  actual_date_return = '".date("Y-m-d")."', active = 0 WHERE id_users = ?;";
      $unsafevariables['id_users'] = htmlspecialchars($_GET['id']);
      $obj_lendedbooks->getFilterLended_books_sqlsafe($sql,$unsafevariables);
    }
  }

  header("Location:../Users.php");
  exit;
}
else{
  header("Location:../Users.php?error=Opravdu chcete smazat uživatele&deleteuser=true");
  $_SESSION['errorurl'] = "src/Formaction/Odebratuzivatele.php?id=".$_GET['id']."&deleteuser=true";
}

}
  else{
    echo '<script>alert("Nemáte oprávnění")</script>';
  }

 ?>
