<?php
if(session_id() == ''){session_start();} if (isset($_SESSION['admin']) AND $_SESSION['admin'] == 1) {
  require_once("../../include.php");

    $obj_books = new Books();
    $obj_books->deleteBook(htmlspecialchars($_POST['id_books']));

    //vratit knihy pod uzivatelem v lended_books
    $obj_lendedbooks = new Lended_books();
    $alllendebooks = $obj_lendedbooks->getAllLended_books();
    foreach ($alllendebooks as $key => $value) {
      if ($value['id_books'] == $_POST['id_books']) {
        $sql = "UPDATE lended_books SET  actual_date_return = '".date("Y-m-d")."', active = 0 WHERE id_books = ?;";
        $unsafevariables['id_books'] = htmlspecialchars($_POST['id_books']);
        $obj_lendedbooks->getFilterLended_books_sqlsafe($sql,$unsafevariables);
      }
    }

    //headertag
    $sql = "SELECT SUM(number) as sum_lendedbooks FROM lended_books WHERE id_users = '".htmlspecialchars($_SESSION['user_id'])."' AND active = '1'";
    $header_tag = $obj_lendedbooks->getFilterLended_books_sql($sql);
    $_SESSION['header_tag'] = $header_tag[0]['sum_lendedbooks'];



    header("Location:../Knihyindex.php?odebrat=true");
  }
    else{
      echo '<script>alert("Nemáte oprávnění")</script>';
    }

?>
