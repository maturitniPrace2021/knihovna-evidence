<?php
if(session_id() == ''){session_start();}
require_once("../../include.php");


$obj_users = new Users();
$obj_users->setAllAttributesby_Id(htmlspecialchars($_POST['id']));
$obj_users->setName(htmlspecialchars($_POST['name']));
$obj_users->setUsername(htmlspecialchars($_POST['username']));
if (isset($_SESSION['admin']) AND $_SESSION['admin'] == 1) {
$obj_users->setAdmin(htmlspecialchars($_POST['admin']));}
if ($_POST['password']!='') {
  if ($_POST['password'] != $_POST['password1']) {
    $_SESSION['errorurl'] = "src/Detail/Edituser.php?id=".$_POST['id']."";
    header("Location:../Detail/Edituser.php?id=".$_POST['id']."&error=Hesla nejsou stejná");
    exit;
  }
  else{
  $obj_users->setPassword(password_hash(htmlspecialchars($_POST['password']), PASSWORD_BCRYPT));
  }
}
$obj_users->updateUser(htmlspecialchars($_POST['id']));

$id = htmlspecialchars($_POST['id']);
if (isset($_SESSION['admin']) AND $_SESSION['admin'] == 1) {
header("Location:../Users.php");
exit;
}
else{
  header("Location:".$filepath."index.php");
  exit;
}
 ?>
