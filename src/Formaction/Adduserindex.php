<?php
if(session_id() == ''){session_start();} if (isset($_SESSION['admin']) AND htmlspecialchars($_SESSION['admin']) == 1) {
require_once("../../include.php");

if ($_POST['password'] != $_POST['password1']) {
  header("Location:../Detail/Adduser.php?error=Hesla nejsou stejná");
  $_SESSION['errorurl'] = "src/Detail/Adduser.php";
  exit;
}
else{
  $obj_users = new Users();
  $allusers = $obj_users->getAllUsers();
  $usernameerror = false;
  foreach ($allusers as $key => $value) {
    if ($value['username'] == $_POST['username']) {
      $usernameerror = true;
      $_SESSION['errorurl'] = "src/Detail/Adduser.php";
      header("Location:../Detail/Adduser.php?error=Již použité přihlašovací jméno");
      exit;
    }
  }
  if (isset($usernameerror) AND $usernameerror == false) {
    $obj_users->setUsername(htmlspecialchars($_POST['username']));
    $obj_users->setName(htmlspecialchars($_POST['name']));
    $obj_users->setPassword(password_hash(htmlspecialchars($_POST['password']), PASSWORD_BCRYPT));
    $obj_users->setAdmin(htmlspecialchars($_POST['admin']));
    $obj_users->insertUser();
    header("Location:../Users.php");
    exit;
  }
}




}
else{
  echo '<script>alert("Nemáte oprávnění")</script>';
}

 ?>
