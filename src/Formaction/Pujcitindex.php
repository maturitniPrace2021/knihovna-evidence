<?php
require_once("../../include.php");

$obj_lendedbooks = new Lended_books();
$obj_lendedbooks->setId_books(htmlspecialchars($_POST['id_books']));

if (isset($_POST['id_users'])) {
  $obj_lendedbooks->setId_users(htmlspecialchars($_POST['id_users']));
}
else{
  $obj_lendedbooks->setId_users(htmlspecialchars($_SESSION['user_id']));
}
$obj_lendedbooks->setDate_lend(htmlspecialchars($_POST['date_lend']));
$obj_lendedbooks->setDate_return(htmlspecialchars($_POST['date_return']));
$obj_lendedbooks->setNumber(htmlspecialchars($_POST['number']));
$obj_lendedbooks->lendbook();


$obj_notes = new Notes();
$obj_notes->setId_lendedbooks($obj_lendedbooks->getId());
$obj_notes->setNote(htmlspecialchars($_POST['note']));
$obj_notes->setLend_return(0);
$obj_notes->insertNote();

//headertag
$obj_lendedbooks = new Lended_books();
$sql = "SELECT SUM(number) as sum_lendedbooks FROM lended_books WHERE id_users = '".htmlspecialchars($_SESSION['user_id'])."' AND active = 1";
$header_tag = $obj_lendedbooks->getFilterLended_books_sql($sql);
$_SESSION['header_tag'] = $header_tag[0]['sum_lendedbooks'];


if (isset($_SESSION['pujcitknihuzanekoho_url']) AND $_SESSION['pujcitknihuzanekoho_url'] == true) {
  header("location:../Knihykdispozici.php?pujcitknihuzanekoho=true");
  unset($_SESSION['pujcitknihuzanekoho_url']);
}
else{
  header("location:../Knihykdispozici.php?pujcit=true");
}
exit;



 ?>
