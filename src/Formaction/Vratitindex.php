<?php

require_once("../../include.php");

$id_lendedbooks = htmlspecialchars($_POST['id_lendedbooks']);
$obj_lendedbooks = new Lended_books();
$obj_lendedbooks->returnbook($id_lendedbooks, date("Y-m-d"));

$id_books = $obj_lendedbooks->getId_books();

$obj_notes = new Notes();
$obj_notes->setId_lendedbooks($obj_lendedbooks->getId());
$obj_notes->setNote(htmlspecialchars($_POST['note']));
$obj_notes->setLend_return(1);
$obj_notes->insertNote();

$obj_lendedbooks = new Lended_books();
$sql = "SELECT SUM(number) as sum_lendedbooks FROM lended_books WHERE id_users = '".htmlspecialchars($_SESSION['user_id'])."' AND active = 1";
$header_tag = $obj_lendedbooks->getFilterLended_books_sql($sql);
$_SESSION['header_tag'] = $header_tag[0]['sum_lendedbooks'];


if (isset($_SESSION['vratitknihuzanekoho_url']) AND $_SESSION['vratitknihuzanekoho_url'] == true) {
  header("location:../Zapujceneknihyall.php?vratitall=true");
  unset($_SESSION['vratitknihuzanekoho_url']);
}
else{
  header("location:../Zapujceneknihy.php?vratit=true");
}
exit;

 ?>
