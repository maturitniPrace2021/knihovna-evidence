<?php
if(session_id() == ''){session_start();} if (isset($_SESSION['admin']) AND $_SESSION['admin'] == 1) {
    require "../vendor/autoload.php";
    require_once("../include.php");

    $latte = new Latte\Engine;

    $latte->setTempDirectory('temp');
    $obj_users = new Users();

    if (isset($_GET['search'])) {
    $unsafevariables[0] = "%".htmlspecialchars($_GET['search'])."%";
    $unsafevariables[1] = "%".htmlspecialchars($_GET['search'])."%";

    $sql = "SELECT id,username,name,admin FROM users WHERE (`name` LIKE ?) OR (`username` LIKE ?)";
    $users = $obj_users->getsqlsafe($sql,$unsafevariables);
    }
    else {
      $sql = "SELECT id,username,name,admin FROM users";
      $users = $obj_users->getsql($sql);
    }

    //autocomplete
    $sql = "SELECT id,username,name,admin FROM users";
    $listBooks_autocomplete = $obj_users->getsql($sql);
    foreach ($listBooks_autocomplete as $key => $value) {
      foreach ($value as $key2 => $value2) {
        if ($key2 == "name" OR $key2 == "username" OR $key2 == "admin") {
          $autocomplete[] = $value2;
        }
      }
    }
    $_SESSION['autocomplete'] = $autocomplete;

    $params = ['filepath' => $filepath,'users' => $users
    ];

    // kresli na výstup
    $latte->render('../template/Index/Usersindex.latte', $params);
}
  else{
    echo '<script>alert("Nemáte oprávnění")</script>';
  }

?>
